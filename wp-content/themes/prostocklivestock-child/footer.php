

			<?php do_action('__footer'); ?>

		</div><!-- end of wrapper -->

	    <?php do_action('__after_wrapper'); ?>

		<?php wp_footer(); ?>
		 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
  

  
  <div class="modal fade frontpage_login" id="myModal" role="dialog">
    <div class="modal-dialog">
    
  
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Login to Prostock</h4>
        </div>
        <p id="error-text" style="color:red"></p>
        <div class="modal-body">
        <?php wp_login_form(); ?>
       </div>
       <?php $websiteurl= esc_url( home_url( '/' ) ); ?>
        <!--<div class="modal-body">
			 <form id="loginform" action="">
			    <div class="form-group">
			      <label for="email">Username:</label>
			      <input type="text" class="form-control" id="username" placeholder="Enter Username" name="username" required>
			    </div>
			    <div class="form-group">
			      <label for="pwd">Password:</label>
			      <input type="password" class="form-control" id="password" placeholder="Enter password" name="password" required>
			    </div>
			    <p id="error-text" style="color:red"></p>
			    <button type="submit" class="btn btn-default" id="loginsubmit">Submit</button>
			  </form>
        </div>-->
        
      </div>
      
    </div>
  </div>

<script>
  $(document).ready(function(){
  	var i =0;

  	$('#wp-submit').click(function(e){
        
	  	if(i == 0){
	  	e.preventDefault();
        $('#error-text').text('');
  		var username=$('#user_login').val();
  		var password=$('#user_pass').val();

	  	$.ajax({
        url: "<?php echo $websiteurl;?>wp-content/themes/prostocklivestock-child/ajaxfile.php",
	        type: "post",
	        data: {username:username,password:password} ,
	        success: function (response) {
	            if(response=="success")
	            {
	            	i = 1;
	            	$('#wp-submit').click();
	            }else{

                    $('#error-text').text('Username/Password is incorrect');
	            }     

	        },
	        error: function() {
	           
	        }


	    });
	  }
    });

   });
</script>

	</body>
</html>
