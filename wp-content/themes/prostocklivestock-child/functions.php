<?php

add_action('wp_enqueue_scripts', 'import_parent_style');

function import_parent_style()
{
    $suffix = dahz_get_min_suffix();
    wp_enqueue_style('df-layout', THEME_URI . '/includes/assets/css/layout' . $suffix . '.css', NULL, NULL);
    wp_enqueue_style('df-layout-child', CHILD_THEME_URI . '/style.css', array(
        'df-layout'
    ), NULL);
}
//custome post for ads For Sale
function custom_post_type()
{
     $labels = array(
     'name' => _x('Ads', 'Post Type General Name', 'techindustan'),
     'singular_name' => _x('Ad', 'Post Type Singular Name', 'techindustan'),
     'menu_name' => __('Ads For Sale', 'techindustan'),
     'parent_item_colon' => __('Parent Property', 'techindustan'),
      'all_items' => __('All Ads', 'techindustan'),
      'view_item' => __('View Ad', 'techindustan'),
      'add_new_item' => __('Add New Ads', 'techindustan'),
       'add_new' => __('Add New For Sale Ad', 'techindustan'),
        
        'edit_item' => __('Edit Ads', 'techindustan'),
        
        'update_item' => __('Update Ads', 'techindustan'),
        
        'search_items' => __('Search Ads', 'techindustan'),
        
        'not_found' => __('Not Found', 'techindustan'),
        
        'not_found_in_trash' => __('Not found in Trash', 'techindustan')
        
    );
    $args   = array(
        
        'label' => __('ads', 'techindustan'),
        
        'description' => __('Ads news and reviews', 'techindustan'),
        
        'labels' => $labels,
        
        'supports' => array(
            'title',
            'editor',
            'thumbnail'
        ),
        
        'taxonomies' => array(
            'genres'
        ),
        
        'hierarchical' => true,
        
        'title_li' => __('Categories'),
        
        'public' => true,
        
        'show_ui' => true,
        
        'show_in_menu' => true,
        
        'show_in_nav_menus' => true,
        
        'show_in_admin_bar' => true,
        
        'menu_position' => 5,
        
        'can_export' => true,
        
        'has_archive' => true,
        
        'exclude_from_search' => true,
        
        'publicly_queryable' => true,
        
        'capability_type' => 'page'
        
        
    );
    
    register_post_type('ads', $args);
    
}
add_action('init', 'custom_post_type');

function create_book_tax()
{
    
    register_taxonomy('adscategory', 'ads', array(
        
        'label' => __('Add Categories'),
        
        'rewrite' => array(
            'slug' => 'ads_category'
        ),
        
        'hierarchical' => true
        
    ));
    
}
add_action('init', 'create_book_tax');
//custome post for ads livestock
function custom_post_type_livestock()
{
    
    $labels = array(
        
        'name' => _x('Livestock', 'Post Type General Name', 'techindustan'),
        
        'singular_name' => _x('Livestock', 'Post Type Singular Name', 'techindustan'),
        
        'menu_name' => __('Ads Livestock', 'techindustan'),
        
        'parent_item_colon' => __('Parent Property', 'techindustan'),
        
        'all_items' => __('All Livestock Ads', 'techindustan'),
        
        'view_item' => __('View Livestock Ad', 'techindustan'),
        
        'add_new_item' => __('Add New Ads', 'techindustan'),
        
        'add_new' => __('Add New  Ad', 'techindustan'),
        
        'edit_item' => __('Edit Livestock Ads', 'techindustan'),
        
        'update_item' => __('Update Livestock Ads', 'techindustan'),
        
        'search_items' => __('Search Livestock Ads', 'techindustan'),
        
        'not_found' => __('Not Found', 'techindustan'),
        
        'not_found_in_trash' => __('Not found in Trash', 'techindustan')
        
    );
    $args   = array(
        
        'label' => __('livestock', 'techindustan'),
        
        'description' => __('Ads news and reviews', 'techindustan'),
        
        'labels' => $labels,
        
        'supports' => array(
            'title',
            'editor',
            'thumbnail'
        ),
        
        'taxonomies' => array(
            'genres'
        ),
        
        'hierarchical' => true,
        
        'public' => true,
        
        'show_ui' => true,
        
        'show_in_menu' => true,
        
        'show_in_nav_menus' => true,
        
        'show_in_admin_bar' => true,
        
        'menu_position' => 5,
        
        'can_export' => true,
        
        'has_archive' => true,
        
        'exclude_from_search' => true,
        
        'publicly_queryable' => true,
        
        'capability_type' => 'page'
        
    );
    
    register_post_type('livestock', $args);
    
}
add_action('init', 'custom_post_type_livestock');
//end of custome post for ads livestock

//custome post for ads Online Auction
function custom_post_type_auction()
{
    
    $labels = array(
        
        'name' => _x('Online Auction', 'Post Type General Name', 'techindustan'),
        
        'singular_name' => _x('Online Auction', 'Post Type Singular Name', 'techindustan'),
        
        'menu_name' => __('Ads Online Auction', 'techindustan'),
        
        'parent_item_colon' => __('Parent Property', 'techindustan'),
        
        'all_items' => __('All Auction Ads', 'techindustan'),
        
        'view_item' => __('View Auction Ad', 'techindustan'),
        
        'add_new_item' => __('Add New Auction Ads', 'techindustan'),
        
        'add_new' => __('Add New Auction Ad', 'techindustan'),
        
        'edit_item' => __('Edit Auction Ads', 'techindustan'),
        
        'update_item' => __('Update Auction Ads', 'techindustan'),
        
        'search_items' => __('Search Auction Ads', 'techindustan'),
        
        'not_found' => __('Not Found', 'techindustan'),
        
        'not_found_in_trash' => __('Not found in Trash', 'techindustan')
        
    );
    $args   = array(
        
        'label' => __('auction', 'techindustan'),
        
        'description' => __('Ads news and reviews', 'techindustan'),
        
        'labels' => $labels,
        
        'supports' => array(
            'title',
            'editor',
            'thumbnail'
        ),
        
        'taxonomies' => array(
            'genres'
        ),
        
        'hierarchical' => true,
        
        'public' => true,
        
        'show_ui' => true,
        
        'show_in_menu' => true,
        
        'show_in_nav_menus' => true,
        
        'show_in_admin_bar' => true,
        
        'menu_position' => 5,
        
        'can_export' => true,
        
        'has_archive' => true,
        
        'exclude_from_search' => true,
        
        'publicly_queryable' => true,
        
        'capability_type' => 'page'
        
    );
    
    register_post_type('auction', $args);
    
}
add_action('init', 'custom_post_type_auction');
//end of custome post for ads auction
// Add a custom user role
$result = add_role('public', __('Public'), array(
    
    'read' => true, // true allows this capability
    'edit_posts' => false, // Allows user to edit their own posts
    'edit_pages' => false, // Allows user to edit pages
    'edit_others_posts' => false, // Allows user to edit others posts not just their own
    'create_posts' => false, // Allows user to create new posts
    'manage_categories' => false, // Allows user to manage post categories
    'publish_posts' => false, // Allows the user to publish, otherwise posts stays in draft mode
    'edit_themes' => false, // false denies this capability. User can’t edit your theme
    'install_plugins' => false, // User cant add new plugins
    'update_plugin' => false, // User can’t update any plugins
    'update_core' => false // user cant perform core updates
    
));

// add aditional field in custom post ads sale unit save and update.

// start meta box read only fields of custom post
add_action('add_meta_boxes', 'ad_id_fields');
function ad_id_fields()
{
    add_meta_box('ad_id', 'Ad ID', 'ad_id_fields_url_meta', 'ads', 'normal', 'low');
    add_meta_box('ad_id', 'Ad ID', 'ad_id_fields_url_meta', 'livestock', 'normal', 'low');
    add_meta_box('ad_id', 'Ad ID', 'ad_id_fields_url_meta', 'auction', 'normal', 'low');
    
}
function ad_id_fields_url_meta($post)
{
    $id = $post->ID;
    
?>
               <strong><?php
    echo $id;
?></strong>
        <?php
}
add_action('add_meta_boxes', 'active_id_fields');
function active_id_fields()
{
    add_meta_box('active_id', 'Active ID', 'active_id_fields_url_meta', 'ads', 'normal', 'low');
    add_meta_box('active_id', 'Active ID', 'active_id_fields_url_meta', 'livestock', 'normal', 'low');
    add_meta_box('active_id', 'Active ID', 'active_id_fields_url_meta', 'auction', 'normal', 'low');
    
}
function active_id_fields_url_meta($post)
{
    $activeid = get_post_meta($post->ID, 'active_id', true);

?>
                    <div class="form-field form-required term-name-wrap">
					<input type="checkbox" name="active" value="active" <?php if($activeid !=''){ echo "checked"; }?> >
                    <input type="hidden" name="add_activeid_noncename" id="add_feature_noncename" value="<?php
    echo wp_create_nonce(plugin_basename(__FILE__) . $post->ID);
?>" />
                  </div>
        <?php
}
function add_ativeid_saving($post_id)
{
    
    global $post;
    
    if (!wp_verify_nonce($_POST['add_activeid_noncename'], plugin_basename(__FILE__) . $post->ID)) {
        return $post->ID;
    }
    
    if (isset($_POST['active'])) {
       echo $active = $_POST['active'];
        update_post_meta($post_id, 'active_id', $active);
    } else {
		 update_post_meta($post_id, 'active_id', '');
        wp_delete_object_term_relationships($post_id, 'genres4');
        
    }
    
}

add_action('save_post', 'add_ativeid_saving');
add_action('add_meta_boxes', 'quantity_fields');
function quantity_fields()
{
   // add_meta_box('quantity', 'Quantity', 'quantity_fields_url_meta', 'ads', 'normal', 'low');
    add_meta_box('quantity', 'Quantity', 'quantity_fields_url_meta', 'livestock', 'normal', 'low');
    add_meta_box('quantity', 'Quantity', 'quantity_fields_url_meta', 'auction', 'normal', 'low');
 }
function quantity_fields_url_meta($post)
{
    $quantity = get_post_meta($post->ID, 'quantity', true);

?>
                    <div class="form-field form-required term-name-wrap">
					<input type="text" name="quantity" value="<?php if($quantity !=''){ echo $quantity; }?>">
                    <input type="hidden" name="add_quantity_noncename" id="add_feature_noncename" value="<?php
    echo wp_create_nonce(plugin_basename(__FILE__) . $post->ID);
?>" />
                  </div>
        <?php
}
function add_quantity_saving($post_id)
{
    
    global $post;
    
    if (!wp_verify_nonce($_POST['add_quantity_noncename'], plugin_basename(__FILE__) . $post->ID)) {
        return $post->ID;
    }
    
    if (isset($_POST['quantity'])) {
       echo $quantity = $_POST['quantity'];
        update_post_meta($post_id, 'quantity', $quantity);
    } else {
		 update_post_meta($post_id, 'quantity', '');
        wp_delete_object_term_relationships($post_id, 'genres4');
        
    }
    
}
add_action('save_post', 'add_quantity_saving');
add_action('add_meta_boxes', 'price_fields');
function price_fields()
{
    add_meta_box('price', 'Price', 'price_fields_url_meta', 'ads', 'normal', 'low');
    //add_meta_box('quantity', 'Quantity', 'quantity_fields_url_meta', 'livestock', 'normal', 'low');
    //add_meta_box('quantity', 'Quantity', 'quantity_fields_url_meta', 'auction', 'normal', 'low');
 }
function price_fields_url_meta($post)
{
    $price = get_post_meta($post->ID, 'price', true);

?>
                    <div class="form-field form-required term-name-wrap">
					<input type="text" name="price" value="<?php if($price !=''){ echo $price; }?>">
                    <input type="hidden" name="add_price_noncename" id="add_feature_noncename" value="<?php
    echo wp_create_nonce(plugin_basename(__FILE__) . $post->ID);
?>" />
                  </div>
        <?php
}
function add_price_saving($post_id)
{
    
    global $post;
    
    if (!wp_verify_nonce($_POST['add_price_noncename'], plugin_basename(__FILE__) . $post->ID)) {
        return $post->ID;
    }
    
    if (isset($_POST['price'])) {
       echo $quantity = $_POST['price'];
        update_post_meta($post_id, 'price', $quantity);
    } else {
		 update_post_meta($post_id, 'price', '');
        wp_delete_object_term_relationships($post_id, 'genres4');
        
    }
    
}

add_action('save_post', 'add_price_saving');
add_action('add_meta_boxes', 'date_created_fields');
function date_created_fields()
{
    add_meta_box('date_created', 'Date Created', 'date_created_fields_url_meta', 'ads', 'normal', 'low');
    add_meta_box('date_created', 'Date Created', 'date_created_fields_url_meta', 'livestock', 'normal', 'low');
    add_meta_box('date_created', 'Date Created', 'date_created_fields_url_meta', 'auction', 'normal', 'low');
    
}
function date_created_fields_url_meta($post)
{
    $id        = $post->ID;
    $post_date = get_the_date('F j, Y', $id);
    
?>
                       <strong><?php
    echo $post_date;
?></strong>
                <?php
}

add_action('add_meta_boxes', 'created_by_fields');
function created_by_fields()
{
    add_meta_box('created_by', 'Created By', 'created_by_fields_url_meta', 'ads', 'normal', 'low');
    add_meta_box('created_by', 'Created By', 'created_by_fields_url_meta', 'livestock', 'normal', 'low');
    add_meta_box('created_by', 'Created By', 'created_by_fields_url_meta', 'auction', 'normal', 'low');
    
}
function created_by_fields_url_meta($post)
{
    $id         = $post->ID;
    $created_by = get_userdata($post->post_author);
    
?>        
                    <strong> <?php
    echo $created_by->display_name;
?></strong>
                <?php
}

add_action('add_meta_boxes', 'modified_date_fields');
function modified_date_fields()
{
    add_meta_box('modified_date', 'Date Modified', 'modified_date_fields_url_meta', 'ads', 'normal', 'low');
    add_meta_box('modified_date', 'Date Modified', 'modified_date_fields_url_meta', 'livestock', 'normal', 'low');
    add_meta_box('modified_date', 'Date Modified', 'modified_date_fields_url_meta', 'auction', 'normal', 'low');
    
}
function modified_date_fields_url_meta($post)
{
    $id            = $post->ID;
    $modified_date = get_the_modified_date('F j, Y', $id);
    
?>        
                        <strong> <?php
    echo $modified_date;
?></strong>
                        <?php
}
add_action('add_meta_boxes', 'modified_by_fields');
function modified_by_fields()
{
    add_meta_box('modified_by', 'Modified By', 'modified_by_fields_url_meta', 'ads', 'normal', 'low');
    add_meta_box('modified_by', 'Modified By', 'modified_by_fields_url_meta', 'livestock', 'normal', 'low');
    add_meta_box('modified_by', 'Modified By', 'modified_by_fields_url_meta', 'auction', 'normal', 'low');
    
}
function modified_by_fields_url_meta($post)
{
    $id          = $post->ID;
    $modified_by = get_the_modified_author();
?>        
                                         <strong><?php
    echo $modified_by;
?></strong>
                                    <?php
}

// End meta box read only fields of custom post

add_action('add_meta_boxes', 'c3m_sponsor_meta');
function c3m_sponsor_meta()
{
    add_meta_box('c3m_meta', 'Sale Unit', 'c3m_sponsor_url_meta', 'ads', 'normal', 'low');
    add_meta_box('c3m_meta', 'Sale Unit', 'c3m_sponsor_url_meta', 'livestock', 'normal', 'low');
    add_meta_box('c3m_meta', 'Sale Unit', 'c3m_sponsor_url_meta', 'auction', 'normal', 'low');
}

function c3m_sponsor_url_meta($post)
{
    $sale_unit = get_post_meta($post->ID, 'sale_unit', true);
    global $wpdb;
    $table_name = $wpdb->prefix . 'sale_units';
    $sql2       = "SELECT * FROM $table_name";
    $resuth1    = $wpdb->get_results($sql2);
?>
                    <div class="form-field form-required term-name-wrap">
                     <select name="sunit"><option value="">All</option>   <?php
    foreach ($resuth1 as $data) {
?><option value="<?php
        echo $data->Name;
?>" <?php
        if ($sale_unit == $data->Name) {
            echo 'selected';
        }
?>><?php
        echo $data->Name;
?></option><?php
    }
?></select>
                    <input type="hidden" name="add_sunit_noncename" id="add_feature_noncename" value="<?php
    echo wp_create_nonce(plugin_basename(__FILE__) . $post->ID);
?>" />
                  </div>
        <?php
}

function add_sunit_saving($post_id)
{
    
    global $post;
    
    if (!wp_verify_nonce($_POST['add_sunit_noncename'], plugin_basename(__FILE__) . $post->ID)) {
        return $post->ID;
    }
    
    if (isset($_POST['sunit'])) {
        $sunit = $_POST['sunit'];
        update_post_meta($post_id, 'sale_unit', $sunit);
    } else {
        wp_delete_object_term_relationships($post_id, 'genres4');
        
    }
    
}

add_action('save_post', 'add_sunit_saving');

// End of add aditional field in custom post ads save and update.

// add aditional field in custom post ads agent save and update.

add_action('add_meta_boxes', 'get_agents');
function get_agents()
{
    add_meta_box('get_agents', 'Agents', 'get_agents_url_meta', 'ads', 'normal', 'low');
    // add_meta_box( 'get_agents', 'Agents', 'get_agents_url_meta', 'livestock', 'normal', 'low' );
    // add_meta_box( 'get_agents', 'Agents', 'get_agents_url_meta', 'auction', 'normal', 'low' );
}
add_action('add_meta_boxes', 'get_sellers');
function get_sellers()
{
    add_meta_box('get_sellers', 'Seller', 'get_sellers_url_meta', 'ads', 'normal', 'low');
}
add_action('add_meta_boxes', 'get_buyers');
function get_buyers()
{
    add_meta_box('get_buyers', 'Buyers', 'get_buyers_url_meta', 'ads', 'normal', 'low');
}
function get_buyers_url_meta($post)
{
    $agent   = get_post_meta($post->ID, 'buyers', true);
    $resuth1 = get_users('orderby=meta_value&meta_key=first_name');
?>
                 <div class="form-field form-required term-name-wrap">
                  <select name="buyers"><option value="">All</option>   <?php
    foreach ($resuth1 as $data) {
        $buyerid   = get_user_meta($data->ID, 'buyer_id', true);
        $firstname = get_user_meta($data->ID, 'first_name', true);
        $lastname  = get_user_meta($data->ID, 'last_name', true);
        $name      = $firstname . ' ' . $lastname;
        if ($buyerid != "" && $buyerid != " ") {
?><option value="<?php
            echo $name;
?>" <?php
            if ($agent == $name) {
                echo 'selected';
            }
?>><?php
            echo $name;
?></option><?php
        }
    }
?></select>
                 <input type="hidden" name="add_buyers_noncename" id="add_feature_noncename" value="<?php
    echo wp_create_nonce(plugin_basename(__FILE__) . $post->ID);
?>" />
               </div>
     <?php
}
function get_agents_url_meta($post)
{
    $agent   = get_post_meta($post->ID, 'agents', true);
    $resuth1 = get_users('orderby=meta_value&meta_key=first_name');
?>
                    <div class="form-field form-required term-name-wrap">
                     <select name="agents"><option value="">All</option>   <?php
    foreach ($resuth1 as $data) {
        $agentId   = get_user_meta($data->ID, 'agent_id', true);
        $firstname = get_user_meta($data->ID, 'first_name', true);
        $lastname  = get_user_meta($data->ID, 'last_name', true);
        $name      = $firstname . ' ' . $lastname;
        if ($agentId != "" && $agentId != " ") {
?><option value="<?php
            echo $name;
?>" <?php
            if ($agent == $name) {
                echo 'selected';
            }
?>><?php
            echo $name;
?></option><?php
        }
    }
?></select>
                    <input type="hidden" name="add_agents_noncename" id="add_feature_noncename" value="<?php
    echo wp_create_nonce(plugin_basename(__FILE__) . $post->ID);
?>" />
                  </div>
        <?php
}
function get_sellers_url_meta($post)
{
    $agent   = get_post_meta($post->ID, 'sellers', true);
    $resuth1 = get_users('orderby=meta_value&meta_key=first_name');
?>
             <div class="form-field form-required term-name-wrap">
              <select name="sellers"><option value="">All</option>   <?php
    foreach ($resuth1 as $data) {
        $sellerid  = get_user_meta($data->ID, 'seller_id', true);
        $firstname = get_user_meta($data->ID, 'first_name', true);
        $lastname  = get_user_meta($data->ID, 'last_name', true);
        $name      = $firstname . ' ' . $lastname;
        if ($sellerid != "" && $sellerid != " ") {
?><option value="<?php
            echo $name;
?>" <?php
            if ($agent == $name) {
                echo 'selected';
            }
?>><?php
            echo $name;
?></option><?php
        }
    }
?></select>
             <input type="hidden" name="add_sellers_noncename" id="add_feature_noncename" value="<?php
    echo wp_create_nonce(plugin_basename(__FILE__) . $post->ID);
?>" />
           </div>
 <?php
}
function add_buyers_saving($post_id)
{
    
    global $post;
    
    if (!wp_verify_nonce($_POST['add_buyers_noncename'], plugin_basename(__FILE__) . $post->ID)) {
        return $post->ID;
    }
    
    if (isset($_POST['buyers'])) {
        $agents = $_POST['buyers'];
        update_post_meta($post_id, 'buyers', $agents);
    } else {
        wp_delete_object_term_relationships($post_id, 'genres4');
        
    }
    
}
add_action('save_post', 'add_buyers_saving');

function add_agents_saving($post_id)
{
    
    global $post;
    
    if (!wp_verify_nonce($_POST['add_agents_noncename'], plugin_basename(__FILE__) . $post->ID)) {
        return $post->ID;
    }
    
    if (isset($_POST['agents'])) {
        $agents = $_POST['agents'];
        update_post_meta($post_id, 'agents', $agents);
    } else {
        wp_delete_object_term_relationships($post_id, 'genres4');
        
    }
    
}
add_action('save_post', 'add_agents_saving');
function add_sellers_saving($post_id)
{
    
    global $post;
    
    if (!wp_verify_nonce($_POST['add_sellers_noncename'], plugin_basename(__FILE__) . $post->ID)) {
        return $post->ID;
    }
    
    if (isset($_POST['sellers'])) {
        $agents = $_POST['sellers'];
        update_post_meta($post_id, 'sellers', $agents);
    } else {
        wp_delete_object_term_relationships($post_id, 'genres4');
        
    }
    
}

add_action('save_post', 'add_sellers_saving');
// End of add aditional field in custom post ads save and update.
//Add New city and state in users area field
function epx_add_user_form_to_admin_area()
{
    global $wpdb;
    $table_country = $wpdb->prefix . 'countries';
    $sql           = "SELECT * FROM $table_country";
    $result        = $wpdb->get_results($sql);
    $table_state = $wpdb->prefix . 'states';
    $aust        = "SELECT * FROM $table_state WHERE `country_id` = 13";
    $resultstate = $wpdb->get_results($aust);
    //echo "<pre>";   print_r($result); die;
    //$country =  get_user_meta($user_id, country, true);  
?>
<div class="country">
<table class="form-table">
<tbody>
<tr>
<th valign="top" scope="row"><label>Select Country</label></th>
<td><div class="acf-input-wrap">   
  <select id="country" name="country">
        <?php
    foreach ($result as $country) {
?>
       <option value="<?php
        echo $country->id;
?>" <?php
        if ($country->id == 13) {
            echo 'selected';
        }
?>><?php
        echo $country->name;
?></option>
        <?php
    }
?>
       </select>
        </div></td>
</tr>
<tr>
<th valign="top" scope="row"><label>Select State</label></th>
<td><div class="acf-input-wrap">   
  <select id="state" name="state">
        <?php
    foreach ($resultstate as $state) {
?>
       <option value="<?php
        echo $state->id;
?>" ><?php
        echo $state->name;
?></option>
        <?php
    }
?>
       </select>
        </div></td>
</tr>
</tbody>
</table>
<?php
    $current_url = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
?>
       </div>
 <script>       
$(document).ready(function(){
   $('body').on('change', '#country', function() {
      var countryid = $( "#country option:selected" ).val();
        $.ajax({
            type:"POST",
            url:"/wp-content/themes/prostocklivestock-child/ajax.php",
            data:{countryid:countryid},
            success:function(data){
                //alert(data);
                $("#state").html(data);
            }
        });
   });
});
     </script>  
<?php
}
add_action('user_new_form', 'epx_add_user_form_to_admin_area', 489);
//End of Add New city and state in users area field
//Edit city and state in users area field
function edit_user_city_state($user)
{
    $usierid     = $user->ID;
    $current_url = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    /* $usierid= explode("user_id=",$current_url);
    $usierid= $usierid[1];
    $usierid= explode("&",$usierid);
    $usierid=$usierid[0];*/
    global $wpdb;
    $table_country = $wpdb->prefix . 'countries';
    $sql           = "SELECT * FROM $table_country";
    $result        = $wpdb->get_results($sql);
    
    $table_state = $wpdb->prefix . 'states';
    //$countryid= $_POST['country'];
    $count       = get_user_meta($usierid, country, true);
    $stateId     = get_user_meta($usierid, state, true);
    $aust        = "SELECT * FROM $table_state WHERE `country_id` = $count";
    $resultstate = $wpdb->get_results($aust);
    //echo "<pre>";   print_r($result); die;
    
    //$count =  get_user_meta($usierid, country, true);  
?>
<div class="country">
<table class="form-table">
<tbody>
<tr>
<th valign="top" scope="row"><label>Select Country</label></th>
<td><div class="acf-input-wrap">   
  <select id="country" name="country">
        <?php
    foreach ($result as $country) {
?>
       <option value="<?php
        echo $country->id;
?>" <?php
        if ($country->id == $count) {
            echo 'selected';
        }
?>><?php
        echo $country->name;
?></option>
        <?php
    }
?>
       </select>
        </div></td>
</tr>
<tr>
<th valign="top" scope="row"><label>Select State</label></th>
<td><div class="acf-input-wrap">   
  <select id="state" name="state">
        <?php
    foreach ($resultstate as $state) {
?>
       <option value="<?php
        echo $state->id;
?>" <?php
        if ($state->id == $stateId) {
            echo 'selected';
        }
?> ><?php
        echo $state->name;
?></option>
        <?php
    }
?>
       </select>
        </div></td>
</tr>
</tbody>
</table>

        </div>
 <script>       
$(document).ready(function(){
   $('body').on('change', '#country', function() {
      var countryid = $( "#country option:selected" ).val();
        $.ajax({
            type:"POST",
            url:"/wp-content/themes/prostocklivestock-child/ajax.php",
            data:{countryid:countryid},
            success:function(data){
                //alert(data);
                $("#state").html(data);
            }
        });
   });
});
     </script>  
<?php
}
add_action('show_user_profile', 'edit_user_city_state');
add_action('edit_user_profile', 'edit_user_city_state');
//End of Edit city and state in users area field
// Update city state in user
function save_custom_user_profile_fields($user_id)
{
    # again do this only if you can
    if (!current_user_can('manage_options'))
        return false;
    
    # save my custom field
    update_usermeta($user_id, 'country', $_POST['country']);
    update_usermeta($user_id, 'state', $_POST['state']);
}
add_action('user_register', 'save_custom_user_profile_fields');
add_action('profile_update', 'save_custom_user_profile_fields');
// End of Update city state in user
// Remove user roles from admin
function remove_built_in_roles()
{
    global $wp_roles;
    
    $roles_to_remove = array(
        'subscriber',
        'contributor',
        'author'
    );
    
    foreach ($roles_to_remove as $role) {
        if (isset($wp_roles->roles[$role])) {
            $wp_roles->remove_role($role);
        }
    }
}
add_action('admin_menu', 'remove_built_in_roles');
// End of Remove user roles from admin
// Start of Remove user extra fileds from admin
function remove_personal_options()
{
    echo '<script type="text/javascript">jQuery(document).ready(function($) {
  
$(\'form#your-profile > h2:first\').remove(); // remove the "Personal Options" title
  
$(\'form#your-profile tr.user-rich-editing-wrap\').remove(); // remove the "Visual Editor" field
  
$(\'form#your-profile tr.user-admin-color-wrap\').remove(); // remove the "Admin Color Scheme" field
  
$(\'form#your-profile tr.user-comment-shortcuts-wrap\').remove(); // remove the "Keyboard Shortcuts" field
  
$(\'form#your-profile tr.user-admin-bar-front-wrap\').remove(); // remove the "Toolbar" field
  
$(\'form#your-profile tr.user-language-wrap\').remove(); // remove the "Language" field
  
$(\'form#your-profile tr.user-twitter-wrap\').remove(); // remove the "Twitter" field
  
$(\'form#your-profile tr.user-facebook-wrap\').remove(); // remove the "Facebook" field
  
$(\'form#your-profile tr.user-google-wrap\').remove(); // remove the "Google" field
  
$(\'form#your-profile tr.user-instagram-wrap\').remove(); // remove the "Instagram" field

$(\'form#your-profile tr.user-pinterest-wrap\').remove(); // remove the "Pinterest" field
  
$(\'form#your-profile tr.user-nickname-wrap\').hide(); // Hide the "nickname" field
  
$(\'table.form-table tr.user-display-name-wrap\').remove(); // remove the “Display name publicly as” field
  
$(\'table.form-table tr.user-url-wrap\').remove();// remove the "Website" field in the "Contact Info" section
  
$(\'h2:contains("About Yourself"), h2:contains("About the user")\').remove(); // remove the "About Yourself" and "About the user" titles
  
$(\'form#your-profile tr.user-description-wrap\').remove(); // remove the "Biographical Info" field
  
$(\'form#your-profile tr.user-profile-picture\').remove(); // remove the "Profile Picture" field
  
$(\'table.form-table tr.user-url-wrap\').remove();// remove the "website" field in the "Contact Info" section

$(\'table.form-table tr.form-field label[for=url], input#url\').remove();
 
$(\'table.form-table tr.user-yim-wrap\').remove();// remove the "Yahoo IM" field in the "Contact Info" section
 
$(\'table.form-table tr.user-jabber-wrap\').remove();// remove the "Jabber / Google Talk" field in the "Contact Info" section
 
});</script>';
    
}
add_action('admin_head', 'remove_personal_options');
add_action('admin_head-user-edit.php', 'remove_personal_options');
add_action('admin_head-profile.php', 'remove_personal_options');
// End  of Remove user extra fileds from admin
function remove_menus()
{
    if (is_user_logged_in() && current_user_can('editor')) {
// echo '<style>#toplevel_page_vc-welcome { display: none;}</style>';
// echo '<style>#setting-error-tgmpa { display: none;}</style>';
        global $menu;
        $restricted = array(
            __('Visual Composer'),
            __('Tools')
        );
        end($menu);
        while (prev($menu)) {
            $value = explode(' ', $menu[key($menu)][0]);
            if (in_array($value[0] != NULL ? $value[0] : "", $restricted)) {
                unset($menu[key($menu)]);
            }
        }
    }
}
add_action('admin_menu', 'remove_menus');
function hide_update_msg_non_admins(){
     if (!current_user_can( 'manage_options' )) { // non-admin users
            echo '<style>#setting-error-tgmpa>.updated settings-error notice is-dismissible, .update-nag, .updated { display: none; }</style>';
        }
    }
    add_action( 'admin_head', 'hide_update_msg_non_admins');
// Unable to access admin by public user.
function wpse66093_no_admin_access()
{
    $redirect = isset( $_SERVER['HTTP_REFERER'] ) ? $_SERVER['HTTP_REFERER'] : home_url( '/' );
    if ( 
        current_user_can( 'public' )
    )
        exit( wp_redirect( $redirect ) );
}
add_action( 'admin_init', 'wpse66093_no_admin_access', 100 );
// Disable admin bar from public user.
add_action('set_current_user', 'cc_hide_admin_bar');
function cc_hide_admin_bar() {
  if (current_user_can('public')) {
    show_admin_bar(false);
  }else if(current_user_can('editor')){
       show_admin_bar(true);
  }
}

//To Disable Plugin Update Notifications
remove_action('load-update-core.php','wp_update_plugins');
add_filter('pre_site_transient_update_plugins','__return_null');