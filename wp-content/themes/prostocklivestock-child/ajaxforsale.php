<?php
require_once ('../../../wp-config.php');
$year  = date('Y');
$month = date('m');
$day   = date('d');
$today = $year . '' . $month . '' . $day;

$slug = $_POST['slug'];
if($slug =='All'){
    $postArg = array(
    'post_type' => 'ads',
    'posts_per_page' => -1,
    'order' => 'desc',
);
}else{
$postArg = array(
    'post_type' => 'ads',
    'posts_per_page' => -1,
    'order' => 'desc',
    'tax_query' => array(
        array(
            'taxonomy' => 'adscategory',
            'field' => 'slug',
            'terms' => $slug
        )
    )
);
}
$i=1;
$posts   = new wp_query($postArg);

if ($posts->have_posts()):
    while ($posts->have_posts()):
        $posts->the_post();
        $postId      = get_the_ID();
        
        $expiry_date = get_post_meta($postId, 'expiry_date', ture);
        $date_listed = get_post_meta($postId, 'date_listed', ture);
        if($today >= $date_listed ){
        if ($today <= $expiry_date) {
            $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full');
            $url              = get_permalink($postId);
?>
<div class="vc_grid-item vc_clearfix vc_col-sm-4 vc_grid-item-zone-c-bottom vc_grid-term-36 vc_visible-item fadeIn animated"><div class="vc_grid-item-mini vc_clearfix"><div class="vc_gitem-animated-block "><div class="vc_gitem-zone vc_gitem-zone-a vc-gitem-zone-height-mode-auto vc-gitem-zone-height-mode-auto-1-1 vc_gitem-is-link" style="background-image: url(<?php
            echo $featured_img_url;
?>) !important;">
    <a href="<?php
            echo $url;
?>" title="GC Test Ad 4" class="vc_gitem-link vc-zone-link"></a>    <img src="<?php
            echo $featured_img_url;
?>" class="vc_gitem-zone-img" alt="">   <div class="vc_gitem-zone-mini">
            </div>
</div>
</div>
<div class="vc_gitem-zone vc_gitem-zone-c vc_custom_1419240516480">
    <div class="vc_gitem-zone-mini">
        <div class="vc_gitem_row vc_row vc_gitem-row-position-top"><div class="vc_col-sm-12 vc_gitem-col vc_gitem-col-align-left"><div class="vc_custom_heading vc_gitem-post-data vc_gitem-post-data-source-"><h4 style="text-align: left"><?php
            the_title();
?></h4></div><div class="vc_custom_heading vc_gitem-post-data vc_gitem-post-data-source-"><p style="text-align: left"></p><?php
            the_excerpt();
?></p>
<p></p></div><div class="vc_btn3-container vc_btn3-left"><a href="<?php
            echo $url;
?>" class="vc_gitem-link vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-flat vc_btn3-color-juicy-pink" title="Read more">Read more</a></div>
</div></div>    </div>
</div>
</div><div class="vc_clearfix"></div></div>
<?php
       $i++;
        }
        }
    endwhile;
    wp_reset_postdata();
else:
?>

<?php
endif;
if($i==1){
    echo "<div class='no_found'>No data found!</div>";
}
?>