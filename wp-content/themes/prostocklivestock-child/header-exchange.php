<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head <?php dahz_attr('head'); ?>>

	<?php wp_head(); ?>

</head>

<?php do_action( '__before_body' ); ?>

<body <?php dahz_attr('body'); ?>>

	<?php do_action('__before_wrapper'); ?>

	<div id="wrapper" class="hfeed site pusher">
	   <div class="header-wrapper col-full southern-live-header">

		<?php //do_action('__header'); ?>
		<?php
$df_header_inner_classes = implode( " ", apply_filters('df_header_inner_classes', array('df-header-inner' ,'df_container-fluid', 'fluid-width', 'header-southern') ) );
$df_header_navbar_pos = df_options( 'header_navbar_position', dahz_get_default( 'header_navbar_position' ) );
?>
<div class="menu-section">

<?php dahz_get_header( 'topbar' ); // Loads the includes/templates/header/topbar.php template. ?>

<header <?php dahz_attr( 'header' ); ?> class="site-header">

	<div class="<?php echo esc_attr( $df_header_inner_classes ) ?> col-full hide">
	    
	    <div class="col-left">
 <?php dahz_get_header( 'branding2' ); // Loads the includes/templates/header/branding2.php template. ?>
</div>
<div class="col-right site-misc-tools">
	<?php dahz_get_header( 'misc' ) // Loads the includes/templates/header/misc.php template. ?>
</div> 
<div class="<?php echo esc_attr( implode( " ", apply_filters( 'df_menu_alignment', array('menu-align') ) ) ) ?>"> 
 <?php dahz_get_menu( 'primary' ); // Loads the includes/templates/menu/primary.php template. ?>
</div>
  

	</div><!-- end of df-header-inner -->

</header><!-- end of header -->


</div>
 <?php do_action( 'dahztheme_title_controller' ); ?>
</div>