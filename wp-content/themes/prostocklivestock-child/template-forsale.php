<?php
/**
 * Template Name: For Sale
 *
 * The microsite page template.
 *
 */
get_header();

$year  = date('Y');
$month = date('m');
$day   = date('d');
$today = $year . '' . $month . '' . $day;

?>
<div id="content-wrap" class="df_container-fluid fluid-width fluid-max col-full">
<?php
// TO SHOW THE PAGE CONTENTS
while (have_posts()):
    the_post();
?> <!--Because the_content() works only inside a WP Loop -->
        <div class="entry-content-page">
            <?php
    the_content();
?> <!-- Page Content -->
        </div><!-- .entry-content-page -->

    <?php
endwhile; //resetting the page loop
wp_reset_query(); //resetting the page query
?>
<?php
$taxonomy = 'adscategory';
$terms    = get_terms($taxonomy);

if ($terms && !is_wp_error($terms)):
?>
<ul class="vc_grid-filter vc_clearfix vc_grid-filter-default vc_grid-filter-size-md vc_grid-filter-center vc_grid-filter-color-grey" data-vc-grid-filter="adscategory">
    <li class="vc_grid-filter-item vc_active changestatus" attr-slug="All"><span data-vc-grid-filter-value="All">All</li>
<?php
    foreach ($terms as $term) {
?>
           <li class="vc_grid-filter-item  changestatus" attr-slug="<?php
        echo $term->slug;
?>"><span data-vc-grid-filter-value="*"><?php
        echo $term->name;
?></li>
        <?php
    }
?>
       
    </ul>
<?php
endif;
?>
<div id="for-sale">
<?php
if (get_query_var('paged'))
    $paged = get_query_var('paged');
if (get_query_var('page'))
    $paged = get_query_var('page');

$args = array(
    'post_type' => 'ads',
    'posts_per_page' => -1,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'meta_query' => array(
        'relation' => 'AND',
        array(
                'key' => 'date_listed',
                'value' => $today,
                'type' => 'NUMERIC',
                 'compare' => '<='
            ),
        array(
            'key' => 'expiry_date',
            'value' => $today,
            'type' => 'NUMERIC',
            'compare' => '>='
        )
    )
);

$query = new WP_Query($args);

//$query = new WP_Query( array( 'post_type' => 'ads', 'paged' => $paged ) );
 $i=1;
if ($query->have_posts()):
?>
<?php
    while ($query->have_posts()):
        $query->the_post();
        $postId      = get_the_ID();
        $expiry_date = get_post_meta($postId, 'expiry_date', ture);
        $date_listed = get_post_meta($postId, 'date_listed', ture);
       // if($today >= $date_listed ){
       
      //  if ($today <= $expiry_date) {
            $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full');
            $url              = get_permalink($postId);
?>
<div class="vc_grid-item vc_clearfix vc_col-sm-4 vc_grid-item-zone-c-bottom vc_grid-term-36 vc_visible-item fadeIn animated"><div class="vc_grid-item-mini vc_clearfix"><div class="vc_gitem-animated-block "><div class="vc_gitem-zone vc_gitem-zone-a vc-gitem-zone-height-mode-auto vc-gitem-zone-height-mode-auto-1-1 vc_gitem-is-link" style="background-image: url(<?php
            echo $featured_img_url;
?>) !important;">
    <a href="<?php
            echo $url;
?>" title="GC Test Ad 4" class="vc_gitem-link vc-zone-link"></a>    <img src="<?php
            echo $featured_img_url;
?>" class="vc_gitem-zone-img" alt="">   <div class="vc_gitem-zone-mini">
            </div>
</div>
</div>
<div class="vc_gitem-zone vc_gitem-zone-c vc_custom_1419240516480">
    <div class="vc_gitem-zone-mini">
        <div class="vc_gitem_row vc_row vc_gitem-row-position-top"><div class="vc_col-sm-12 vc_gitem-col vc_gitem-col-align-left"><div class="vc_custom_heading vc_gitem-post-data vc_gitem-post-data-source-"><h4 style="text-align: left"><?php
            the_title();
?></h4></div><div class="vc_custom_heading vc_gitem-post-data vc_gitem-post-data-source-"><p style="text-align: left"></p><?php
            the_excerpt();
?></p>
<p></p></div><div class="vc_btn3-container vc_btn3-left"><a href="<?php
            echo $url;
?>" class="vc_gitem-link vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-flat vc_btn3-color-juicy-pink" title="Read more">Read more</a></div>
</div></div>    </div>
</div>
</div><div class="vc_clearfix"></div></div>
<?php
$i++;
   //     }
  //  }
    endwhile;
    wp_reset_postdata();
?>
<!-- show pagination here -->
<?php
else:
    echo "<div class='no_found'>No data found!</div>";
?>
<!-- show 404 error here -->
<?php
endif;
/*if($i==1){
    echo "<div class='no_found'>No data found!</div>";
}*/
?>
</div>
</div>
<?php $websiteurl= esc_url( home_url( '/' ) ); ?>
<script>
    $("body").on('click','.changestatus',function(){  
    $('.changestatus').removeClass('vc_active');
    $(this).addClass('vc_active');
     var slug=$(this).attr('attr-slug');      
     $.ajax({    
     url:"<?php echo $websiteurl; ?>wp-content/themes/prostocklivestock-child/ajaxforsale.php",    
     type: "POST",     
     data:{slug:slug},    
     success: function(data) 
     
     {
         $('#for-sale').html(data);
     },      
     }); 
     });
    </script>
<?php
get_footer();
?>