<?php
/*
Single Post Template: Online Auction
*/
?>
<?php get_header();  ?>
<link rel="stylesheet" href="/wp-content/themes/prostocklivestock-child/slider/flexslider.css" type="text/css" media="screen" />
	<div id="content-wrap" class="df_container-fluid fluid-width fluid-max col-full">

		<div class="df_row-fluid main-sidebar-container">
				
				<div class="df-main col-full df_span-sm-12">
				 <h1> <a href="<?php echo $link; ?>"><?php the_title(); ?></a></h1>
				 </div>
				
			<div class="df-main col-full df_span-sm-6 slider_area" <?php //dahz_attr('content'); ?>>


				<?php if (have_posts()) : // Check if have post. ?>

					<?php
					/**
					 * location function includes/functions/content-blog.php
					 * apply_filters( 'df_blog_class_wrapper', 'blog class' );
					 * ini filter untuk blog layout class dengan priority :
					 * 0 => df_blog_grid_layout
					 * 5 => df_blog_list_index_add_class
					 */ ?>

					<div class="<?php esc_attr( apply_filters( 'df_blog_class_wrapper', 'blog class' ) ); ?>">

					<?php while (have_posts()) : the_post(); // Loads the post data. ?>

						<?php //dahz_get_content_template(); // Loads the includes/templates/content/*.php template. ?>

		<?php	$postID=get_the_ID();
         $quantity= get_post_meta( $postID, 'quantity', true);
         $auctiondate= get_post_meta( $postID, 'auction_date', true);
         if($auctiondate !='' && $auctiondate !=" "){
            $year= substr($auctiondate,0,4);
            $month= substr($auctiondate,4,2);
            $date= substr($auctiondate,6,2);
            $auctiondate=$date."/".$month."/".$year;
         }
         $auctiontime= get_post_meta( $postID, 'auction_time', true);
         $auctiontimezone= get_post_meta( $postID, 'auction_timezone', true);
         $auctionlot= get_post_meta( $postID, 'auction_lot', true);
         $auctionurl= get_post_meta( $postID, 'auction_url', true);
         $lotassessment= get_post_meta( $postID, 'lot_assessment', true);
         if($lotassessment !='' && $lotassessment !=' '){
            $lotassessment = get_the_guid($lotassessment);
         }
         //$price= get_post_meta( $postID, 'price', true);
        $gallery= get_post_meta( $postID, 'additional_image', true);
         $link = get_the_permalink();
         //$category= wp_get_post_terms($postID, 'adscategory', true);
         //$firstCategory = $category[0]->name;
?>
  <div class='cpta-Section'>
    
      <div id="slider" class="flexslider">
  <ul class="slides">
    <li>
      <img src="/wp-content/themes/prostocklivestock-child/timthumb.php?src=<?php the_post_thumbnail_url( 'full' ); ?>&w=772&h=480" />
    </li>
     <?php   if( have_rows('additional_image') ):
        
        // loop through the rows of data
        while ( have_rows('additional_image') ) : the_row();
        
        // display a sub field value
        $url = get_sub_field('additional_image_upalod'); ?>
         
         <li>
          <img src="/wp-content/themes/prostocklivestock-child/timthumb.php?src=<?php echo $url['url'] ;?>&w=772&h=480"/>
    </li><?php
 endwhile;
endif; 
?>
    <!-- items mirrored twice, total of 12 -->
  </ul>
</div>
<div id="carousel" class="flexslider">
  <ul class="slides">
    <li>
       <img src="/wp-content/themes/prostocklivestock-child/timthumb.php?src=<?php the_post_thumbnail_url( 'thumbnail' ); ?>&w=160&h=117" />
    </li>
    <?php   if( have_rows('additional_image') ):
        
        // loop through the rows of data
        while ( have_rows('additional_image') ) : the_row();
        
        // display a sub field value
        $url = get_sub_field('additional_image_upalod');
         //echo $url['url'];
         ?>
         <li>
            
             
      <img src="/wp-content/themes/prostocklivestock-child/timthumb.php?src=<?php echo $url['url'] ;?>&w=160&h=117"/>
    </li> <?php
 endwhile;
endif; 
?>
    <!-- items mirrored twice, total of 12 -->
  </ul>
</div>
          
        
        
         
        </div>

					<?php endwhile; // End of loads the post data. ?>

			   		</div>


				<?php endif; ?>

	   		</div>

        <div class="df-main col-full df_span-sm-6 ">
	<div class="aside_post_meta"><div class="meta_block">
           <span>Quantity : <?php echo $quantity; ?></span>
		  </div>
		  <div class="meta_block even">
         <span> Post ID : <?php echo $postID; ?></span>
        </div></div>	
        <div class="ads_description "><p><?php echo get_the_content(); ?> </p></div>
        
        <div class="aside_post_meta auction"><div class="meta_block ">
           <label>Auction Date :</label> <span><?php echo $auctiondate; ?></span>
		  </div>
		  <div class="meta_block even">
          <label> Auction Time : </label> <span><?php echo $auctiontime; ?> <?php echo $auctiontimezone; ?></span>
        </div>
    <!--    <div class="meta_block">-->
    <!--    <label>Auction Timezone :</label> <span> <?php// echo $auctiontimezone; ?></span>-->
		  <!--</div>-->
		  <div class="meta_block even">
         <label>Auction Lot Number :</label> <span> <?php echo $auctionlot; ?></span>
        </div>
        <div class="meta_block">
          <label>Auction URL :</label> <span> <a href="<?php echo $auctionurl; ?>" target="_blank">View Auction</a></span>
		  </div>
		  <div class="meta_block even">
         <label>Lot Assessment :</label> <span> <a href="<?php echo $lotassessment; ?>" target="_blank">Click here</a></span>
        </div>
        </div>	
</div>
		</div>

	</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.min.js">\x3C/script>')</script>
   <script defer src="/wp-content/themes/prostocklivestock-child/slider/jquery.flexslider.js"></script>
<script>
jQuery(window).load(function() {
  // The slider being synced must be initialized first
  jQuery('#carousel').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 150,
    itemMargin: 5,
    asNavFor: '#slider'
  });
 
  jQuery('#slider').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    sync: "#carousel"
  });
});
</script>
<style>
.auction .meta_block span {
    width: 60%;
    display: inline-block;
    border: none;
}

.auction .meta_block label {
    display: inline-block;
    font-weight: bold;
    width: 38%;
}
.auction .meta_block {
    color: #fff;
    font-size: 16px;
    display: block;
    border: 1px solid #fff;
    padding: 6px 12px;
    margin-bottom: 10px;
}
.aside_post_meta.auction {
    margin-bottom: 10px;
}
.auction .meta_block span a {
    color: #fff;
}
</style>
<?php get_footer(); ?>

