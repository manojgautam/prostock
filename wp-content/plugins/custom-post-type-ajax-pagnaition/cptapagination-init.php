<?php
/*
Plugin Name:CPTA
Description:It's a simple custom post type ajax pagination plugin.
Version:1.1
Author: 
License:GPL2
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

function cpta_pagination_enqueue() {
  wp_enqueue_style( 'cptapagination' ,  plugin_dir_url( __FILE__ ) . 'css/cptapagination-style.css' );
  wp_register_script( 'cpta-pagination-custom-js', plugin_dir_url( __FILE__ ) .'/js/cptapagination.js');
  wp_localize_script( 'cpta-pagination-custom-js', 'ajax_params', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
  wp_enqueue_script( 'cpta-pagination-custom-js' );
}
add_action('wp_enqueue_scripts', 'cpta_pagination_enqueue');

add_action( 'wp_ajax_cptapagination', 'cptapagination_callback' );
add_action( 'wp_ajax_nopriv_cptapagination', 'cptapagination_callback' );         

function cptapagination_callback() {
  global $wpdb;
  $cptaNumber = absint($_POST['number']);
  $cptaLimit  = absint($_POST['limit']);
  $cptaType = sanitize_text_field($_POST['cptapost']);
  $cptaCatName = sanitize_text_field($_POST['cptacatname']);
  $cptataxname = sanitize_text_field($_POST['cptataxname']);
  

$year  = date('Y');
$month = date('m');
$day   = date('d');
$today = $year . '' . $month . '' . $day;

  if( $cptaNumber == "1" ){
    $cptaOffsetValue = "0";
    if( $cptataxname ) {
      $args = array('posts_per_page' => $cptaLimit,'post_type' => $cptaType,$cptataxname=>$cptaCatName,'post_status' => 'publish',   
       'meta_query' => array(
        'relation' => 'AND',
        array(
                'key' => 'date_listed',
                'value' => $today,
                'type' => 'NUMERIC',
                 'compare' => '<='
            ),
        array(
            'key' => 'expiry_date',
            'value' => $today,
            'type' => 'NUMERIC',
            'compare' => '>='
        )
    ));   
    }else{
      $args = array('posts_per_page' => $cptaLimit,'post_type' => $cptaType,'post_status' => 'publish',    'meta_query' => array(
        'relation' => 'AND',
        array(
                'key' => 'date_listed',
                'value' => $today,
                'type' => 'NUMERIC',
                 'compare' => '<='
            ),
        array(
            'key' => 'expiry_date',
            'value' => $today,
            'type' => 'NUMERIC',
            'compare' => '>='
        )
    ));  
    }
  }else{
    $cptaOffsetValue = ($cptaNumber-1)*$cptaLimit;
    if( $cptataxname ) {
    $args = array('posts_per_page' => $cptaLimit,'post_type' => $cptaType,$cptataxname=>$cptaCatName,'offset' => $cptaOffsetValue,'post_status' => 'publish',  
      'meta_query' => array(
        'relation' => 'AND',
        array(
                'key' => 'date_listed',
                'value' => $today,
                'type' => 'NUMERIC',
                 'compare' => '<='
            ),
        array(
            'key' => 'expiry_date',
            'value' => $today,
            'type' => 'NUMERIC',
            'compare' => '>='
        )
    ));
    }else{
    $args = array('posts_per_page' => $cptaLimit,'post_type' => $cptaType,'offset' => $cptaOffsetValue,'post_status' => 'publish',    
     'meta_query' => array(
        'relation' => 'AND',
        array(
                'key' => 'date_listed',
                'value' => $today,
                'type' => 'NUMERIC',
                 'compare' => '<='
            ),
        array(
            'key' => 'expiry_date',
            'value' => $today,
            'type' => 'NUMERIC',
            'compare' => '>='
        )
    )); 
    }
    
  }
  $cptaQuery = new WP_Query( $args );
    echo "<div id='cptapagination-content'>";
    if( $cptaQuery->have_posts() ){
       $i=0;
       while( $cptaQuery->have_posts() ){ $cptaQuery->the_post();
        $postID=get_the_ID();
         $quantity= get_post_meta( $postID, 'quantity', true);
         $price= get_post_meta( $postID, 'price', true);
         $link = get_the_permalink();
         if($i%2 == 0) {   ?>

        <div class='cpta-Section odd'>
         <div class="vc_col-sm-5 cpta_image">
             <img  src= <?php the_post_thumbnail_url( 'large' ); ?>>
         </div>
         <div class="vc_col-sm-7 adverts_section">
            <h2> <a href="<?php echo $link; ?>"><?php the_title(); ?></a></h2>
        <div class="adverts_desc">
              <p><?php echo wp_trim_words( get_the_content(), 25, '...' ); ?> </p>
          </div>
          <div class="price_qty">
          <span>Price  : $ <?php echo number_format($price,2); ?> &nbsp;&nbsp; Quantity : <?php echo $quantity;?></span>
          </div>
              <div class="post_id">
             <span> Post ID : <?php echo $postID?></span>
         </div>
         </div>
      </div>

        <?php } else {?>

      <div class='cpta-Section even'>

       <div class="vc_col-sm-7 adverts_section">
          <h2> <a href="<?php echo $link; ?>"><?php the_title(); ?></a></h2>
          <div class="adverts_desc">
            <p><?php echo wp_trim_words( get_the_content(), 25, '...' ); ?> </p>
          </div>
          <div class="price_qty">
          <span>Price  : $ <?php echo number_format($price,2); ?> &nbsp;&nbsp; Quantity : <?php echo$quantity;?></span>
          </div>
              <div class="post_id">
             <span> Post ID : <?php echo $postID?></span>
         </div>
        </div>
          <div class="vc_col-sm-5 cpta_image">
             <img  src= <?php the_post_thumbnail_url( 'large' ); ?>>
         </div>
      </div>
    <?php  } $i++; } wp_reset_postdata();
    }
    if($cptataxname!=""){
      $cpta_args = array('posts_per_page' => -1,'post_type' => $cptaType,$cptataxname=>$cptaCatName,'post_status' => 'publish',    
     'meta_query' => array(
        'relation' => 'AND',
        array(
                'key' => 'date_listed',
                'value' => $today,
                'type' => 'NUMERIC',
                 'compare' => '<='
            ),
        array(
            'key' => 'expiry_date',
            'value' => $today,
            'type' => 'NUMERIC',
            'compare' => '>='
        )
    ));
    }else{
      $cpta_args = array('posts_per_page' => -1,'post_type' => $cptaType,'post_status' => 'publish',    
     'meta_query' => array(
        'relation' => 'AND',
        array(
                'key' => 'date_listed',
                'value' => $today,
                'type' => 'NUMERIC',
                 'compare' => '<='
            ),
        array(
            'key' => 'expiry_date',
            'value' => $today,
            'type' => 'NUMERIC',
            'compare' => '>='
        )
    ));
    }
    $cpta_Query = new WP_Query( $cpta_args );
    $cpta_Count = count($cpta_Query->posts);
    $cpta_Paginationlist = $cpta_Count/$cptaLimit;
    $last = ceil( $cpta_Paginationlist );
    if( $cptaNumber>1 ){ $cptaprev = $cptaNumber-1; }
    if( $cptaNumber < $last ){ $cptanext = $cptaNumber+1; }
    echo "<ul class='list-cptapagination'>";
    echo "<li class='pagitext'><a href='javascript:void(0);' onclick='javascript:cptaajaxPagination($cptaprev,$cptaLimit)'>Prev</a></li>";
    for( $cpta=1; $cpta<=ceil($cpta_Paginationlist); $cpta++){
      if( $cptaNumber ==  $cpta ){ $active="active"; }else{ $active=""; }
      echo "<li><a href='javascript:void(0);' id='post' class='$active' data-posttype='$cptaType'  data-taxname='$cptataxname'  data-cattype='$cptaCatName' onclick='cptaajaxPagination($cpta,$cptaLimit);'>$cpta</a></li>";
    }
    echo "<li class='pagitext'><a href='javascript:void(0);' onclick='javascript:cptaajaxPagination($cptanext,$cptaLimit)'>Next</a></li>";
    echo "</ul>";
    echo "</div>";
  wp_die();
}

function cptapagination_default($atts) {
  global $wpdb;
  $year  = date('Y');
$month = date('m');
$day   = date('d');
$today = $year . '' . $month . '' . $day;
  
  $atts = shortcode_atts( array('custom_post_type' => '','cptataxname'=>'','cptacatname'=>'','post_limit' => ''),$atts,'cptapagination');
  
  if($atts['custom_post_type'] !="" ){
    $cptaType = sanitize_text_field($atts['custom_post_type']); 
  }else{
    $cptaType ="post";  
  }
  
  if($atts['cptacatname'] !="" ){
    $cptaCatName = sanitize_text_field($atts['cptacatname']); 
  }else{
    $cptaCatName ="uncategorized";  
  }
  
  if( $atts['post_limit'] !="" ){
    $cptaLimit= absint($atts['post_limit']);  
  }else{
    $cptaLimit=5;
  }

  
  if($atts['cptataxname']!=""){
    $cptataxname =  sanitize_text_field($atts['cptataxname']);
    $args = array('posts_per_page' => $cptaLimit,'post_type' => $cptaType,$cptataxname=>$cptaCatName,'post_status' => 'publish',   
       'meta_query' => array(
        'relation' => 'AND',
        array(
                'key' => 'date_listed',
                'value' => $today,
                'type' => 'NUMERIC',
                 'compare' => '<='
            ),
        array(
            'key' => 'expiry_date',
            'value' => $today,
            'type' => 'NUMERIC',
            'compare' => '>='
        )
    )
    );
  }else{
    $args = array('posts_per_page' => $cptaLimit,'post_type' => $cptaType,'post_status' => 'publish',   
       'meta_query' => array(
        'relation' => 'AND',
        array(
                'key' => 'date_listed',
                'value' => $today,
                'type' => 'NUMERIC',
                 'compare' => '<='
            ),
        array(
            'key' => 'expiry_date',
            'value' => $today,
            'type' => 'NUMERIC',
            'compare' => '>='
        )
    ));  
  } 
  
  
  $cptaQuery = new WP_Query( $args );
  echo "<div id='cptapagination-content'>";
    if( $cptaQuery->have_posts() ){
      $i=0;
      while( $cptaQuery->have_posts() ){ $cptaQuery->the_post();
        $postID=get_the_ID();
         $quantity= get_post_meta( $postID, 'quantity', true);
         $link = get_the_permalink();
         $price= get_post_meta( $postID, 'price', true);
         if($i%2 == 0) {  ?>

        <div class='cpta-Section odd'>
    		 <div class="vc_col-sm-5 cpta_image">
             <img  src= <?php the_post_thumbnail_url( 'large' ); ?>>
    		 </div>
    		 <div class="vc_col-sm-7 adverts_section">
            <h2> <a href="<?php echo $link; ?>"><?php the_title(); ?></a></h2>
    		<div class="adverts_desc">
              <p><?php echo wp_trim_words( get_the_content(), 25, '...' ); ?> </p>
    		  </div>
    		  <div class="price_qty">
    		  <span>Price  : $ <?php echo  number_format($price,2); ?> &nbsp;&nbsp; Quantity : <?php echo$quantity;?></span>
    		  </div>
              <div class="post_id">
             <span> Post ID : <?php echo $postID?></span>
    		 </div>
    		 </div>
      </div>

        <?php } else {?>

      <div class='cpta-Section even'>

       <div class="vc_col-sm-7 adverts_section">
          <h2> <a href="<?php echo $link; ?>"><?php the_title(); ?></a></h2>
          <div class="adverts_desc">
            <p><?php echo wp_trim_words( get_the_content(), 25, '...' ); ?> </p>
          </div>
          <div class="price_qty">
          <span>Price  : $ <?php echo  number_format($price,2); ?> &nbsp;&nbsp; Quantity : <?php echo$quantity;?></span>
          </div>
              <div class="post_id">
             <span> Post ID : <?php echo $postID?></span>
         </div>
        </div>
          <div class="vc_col-sm-5 cpta_image">
             <img  src= <?php the_post_thumbnail_url( 'large' ); ?>>
         </div>
      </div>

    <?php } $i++; } wp_reset_postdata();
    }
    $year  = date('Y');
$month = date('m');
$day   = date('d');
$today = $year . '' . $month . '' . $day;
    if($cptataxname!=""){
      $cpta_args = array('posts_per_page' => -1,'post_type' => $cptaType,$cptataxname=>$cptaCatName,'post_status' => 'publish',   
       'meta_query' => array(
        'relation' => 'AND',
        array(
                'key' => 'date_listed',
                'value' => $today,
                'type' => 'NUMERIC',
                 'compare' => '<='
            ),
        array(
            'key' => 'expiry_date',
            'value' => $today,
            'type' => 'NUMERIC',
            'compare' => '>='
        )
    ));
    }else{
      $cpta_args = array('posts_per_page' => -1,'post_type' => $cptaType,'post_status' => 'publish',   
       'meta_query' => array(
        'relation' => 'AND',
        array(
                'key' => 'date_listed',
                'value' => $today,
                'type' => 'NUMERIC',
                 'compare' => '<='
            ),
        array(
            'key' => 'expiry_date',
            'value' => $today,
            'type' => 'NUMERIC',
            'compare' => '>='
        )
    ));
    }
    $cpta_Query = new WP_Query( $cpta_args );
    $cpta_Count = count($cpta_Query->posts);
    $cpta_Paginationlist = $cpta_Count/$cptaLimit;
    echo "<ul class='list-cptapagination'>";
    echo "<li class='pagitext'><a href='javascript:void(0);' onclick='javascript:cptaajaxPagination(1,$cptaLimit)'>Prev</a></li>";
    for( $cpta=1; $cpta<=ceil($cpta_Paginationlist); $cpta++){ 
      if( $cpta ==  0 || $cpta ==  1 ){ $active="active"; }else{ $active=""; }
      echo "<li><a href='javascript:void(0);' id='post' class='$active' data-posttype='$cptaType' data-taxname='$cptataxname' data-cattype='$cptaCatName' onclick='cptaajaxPagination($cpta,$cptaLimit);'>$cpta</a></li>";
    }
    echo "<li class='pagitext'><a href='javascript:void(0);' onclick='javascript:cptaajaxPagination(2,$cptaLimit)'>Next</a></li>";
    echo "</ul>";
  echo "</div>";
}
add_shortcode( 'cptapagination', 'cptapagination_default' );
?>