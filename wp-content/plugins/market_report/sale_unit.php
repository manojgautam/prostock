<?php
   global $wpdb;
?>
    <script>
        $(document).ready(function() {
            $(".notice-dismiss").click(function() {
                $(this).parent.hide();
            });
        });
    </script>
    <?php
$unit_table = $wpdb->prefix."sale_units";
if( isset( $_POST['breed'] ) ) {   
    $name=$_POST['breedname'];
    $active=($_POST['active']==""?'false':'true');


    $sql = "INSERT INTO $unit_table        
        (`Name`,`Active`)VALUES        
        ('$name','$active')";

    if( $wpdb->query( $sql ) ) {
         echo "<script>jQuery(document).ready(function(){ jQuery('#setting-error-settings_updated').addClass('updated'); jQuery('#setting-error-settings_updated').removeClass('error'); jQuery('#setting-error-settings_updated').show(); jQuery('#setting-error-settings_updated').find('strong').text('Data Added Successfully.'); });</script>";
    
    }
}
if( isset( $_POST['update'] ) ) {   
    $name=$_POST['breedname'];
    $active=($_POST['active']==""?'false':'true');
    $id= $_POST['breedid'];
    
        $sql = $wpdb->prepare("UPDATE `".$unit_table."` SET `Name` = %s, `Active` = %s WHERE Id = $id", $name, $active );
    
    if( $wpdb->query( $sql ) ) {
         echo "<script>jQuery(document).ready(function(){ jQuery('#setting-error-settings_updated').addClass('updated'); jQuery('#setting-error-settings_updated').removeClass('error'); jQuery('#setting-error-settings_updated').show(); jQuery('#setting-error-settings_updated').find('strong').text('Updated Successfully.'); });</script>";
    
    }
}
if( isset( $_POST['delete'] ) ) {  
    $id= $_POST['stockid'];
$del = $wpdb->query(
              'DELETE  FROM '.$unit_table.'
               WHERE Id = "'.$id.'"'
);
 echo "<script>jQuery(document).ready(function(){ jQuery('#setting-error-settings_updated').addClass('updated'); jQuery('#setting-error-settings_updated').removeClass('error'); jQuery('#setting-error-settings_updated').show(); jQuery('#setting-error-settings_updated').find('strong').text('Record Deleted.'); });</script>";
  
}


if( isset($_POST['inactive'])){
$id= $_POST['stockid'];
$active = "false";

$inactivebreed = $wpdb->prepare("UPDATE $unit_table SET `Active` = %s WHERE Id = $id",  $active );
$wpdb->query( $inactivebreed );
echo "<script>jQuery(document).ready(function(){ jQuery('#setting-error-settings_updated').addClass('updated'); jQuery('#setting-error-settings_updated').removeClass('error'); jQuery('#setting-error-settings_updated').show(); jQuery('#setting-error-settings_updated').find('strong').text('Record Updated.'); });</script>";

}

?>

        <div id="wpbody" role="main" class="marketreport">
            <div id="wpbody-content" aria-label="Main content" tabindex="0">
                <div class="wrap nosubsub">
                    <div id="col-container" class="wp-clearfix">
                        <div id="col-left">
                            <div class="col-wrap">
                                <div class="form-wrap">
         <div id="setting-error-settings_updated" class="updated settings-error notice is-dismissible" style="display:none;"> 
         <p>
             <strong></strong>
         </p>
         <button type="button" class="notice-dismiss">
             <span class="screen-reader-text">Dismiss this notice.</span>
         </button>
     </div>
                                    
                                    <form method="post" action="" enctype="multipart/form-data" id="addtag" class="validate">
                                    <h1 class="wp-heading-inline">Add Sale Unit</h1>
                                        <div class="form-field form-required term-name-wrap">
                                            <label for="tag-name">Sale Units Name</label>
                                            <input name="breedname" id="tag-name" type="text" value="" size="40" required/>
                                            <p>The name is how it appears on your site.</p>
                                        </div>
                                        <div class="form-field form-required term-name-wrap">
                                            <label for="tag-name">Active</label>
                                            <input type="checkbox" name="active" value="true">
                                            <p>Checked for active uncheck for inactive</p>
                                        </div>
                                        <p class="submit"><input type="submit" name="breed" id="submit" class="button button-primary" value="Add New Unit"></p>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <?php 
    global $wpdb;
    $table_name= $wpdb->prefix .'sale_units';
    $sql2="SELECT * FROM $table_name" ;
                 $resuth1=$wpdb->get_results($sql2);
                 $resuth2= $wpdb->num_rows;
                // echo "<pre>"; print_r($resuth1); die;
?>


                        <div id="col-right">
                            <div class="col-wrap">
                                <div class="wrap">
                                    <h1> Sale Unit Data</h1>
                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Active</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Name</th>
                                                <th>Active</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <?php 
       foreach($resuth1 as $data){
           $name=$data->Name;
           $ads_for = array(
                'post_type' => array('ads', 'livestock', 'auction'),
                  'meta_query'=> array(
                      array(
                        'key' => 'sale_unit',
                        'value' => $name,
                        'compare' => '='
                    )
                ),
                );
$ads_for = new WP_Query( $ads_for );
  $count = $ads_for->post_count;
            echo "<tr>";;
            echo "<td>".$data->Name."</td>";
             echo "<td>".$data->Active."</td>";
            echo "<td><span class='edit'><a href='#modal_$data->Id'>Edit</a> |</span> <span class='trash'><a href='#del' class='sale-del' attr-id='$data->Id' attr-count='$count' attr-active='$data->Active'>Delete</a><span></td>";
            echo "</tr>"; ?>
                                            <!---- Remodal---->
                                            <div class="remodal" data-remodal-id="modal_<?php echo $data->Id ;?>" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
                                                <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
                                                <div>
                                                    <h2 id="modal1Title">Update Sale Units</h2>
                                                    <form method="post" action="" enctype="multipart/form-data">
                                                        <div class="form-field form-required term-name-wrap market_form">
                                                            <label for="tag-name">Stock Type Name</label>
                                                            <input name="breedname" id="tag-name" type="text" value="<?php echo $data->Name; ?>" size="40" aria-required="true">
                                                            <input type="hidden" name="breedid" value="<?php echo $data->Id; ?>">
                                                        </div>
                                                        <div class="form-field form-required term-name-wrap market_form">
                                                            <label for="tag-name">Active</label>
                                                            <input type="checkbox" name="active" value="<?php echo $data->Active; ?>" <?php if($data->Active == 'true'){ echo 'checked';} ?>>
                                                        </div>
                                                        <p class="submit">
                                                            <input type="submit" name="update" id="submit" class="remodal-confirm" value="Update Unit">
                                                            <button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
                                                        </p>
                                                    </form>
                                                </div>

                                            </div>
                                            <!---- Remodal end ---->
                                            <?php  }     ?>
                                        </tbody>
                                    </table>

                                    <!----Delete Remodal---->
                                    <div class="remodal" data-remodal-id="del" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
                                        <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
                                        <form method="post" action="" enctype="multipart/form-data">
                                            <div class="form-field form-required term-name-wrap">
                                                <input type="hidden" name="stockid" id="stock-id" value="">
                                            </div>
                                            <div>
                                            <div class="del">
                                            <h2 id="modal1Title">Are you sure?</h2>
                                            <p id="modal1Desc">
                                             You will not be able to recover this Sale Unit.
                                            </p></div>

                                            <div class="inactive">
                                            <h2>Delete Not Possible</h2>
                                            <p>There are <span class="recordcount"></span> records attached. Set it to Inactive instead.</p>
                                            </div>

                                            <div class="active">
                                            <h2>Delete Not Possible</h2>
                                            <p>There are <span class="recordcount"></span> records attached.</p>
                                            </div>
                                           </div>
                                    <br>
                                    <button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
                                    <input type="submit" name="delete" id="submit" class="remodal-confirm del" value="Yes, delete it!">
                                    <input type="submit" name="inactive" id="submit" class="remodal-confirm inactive" value="Make Inactive">
                                </form>
                                    </div>

                                </div>
                                <!-- <script>
                                    jQuery(document).ready(function() {
                                        jQuery('.sale-del').click(function() {
                                            var bid = $(this).attr('attr-id');
                                            jQuery('#sale-id').val(bid);
                                        });
                                    });
                                </script> -->
                                <script>
                                    jQuery(document).ready(function() {
                                        jQuery('.sale-del').click(function() {
                                            var bid = $(this).attr('attr-id');
                                            var count = $(this).attr('attr-count');
                                            var active = $(this).attr('attr-active');
                                            
                                            if(count>0){
                                                if(active == 'true'){
                                                    $(".inactive").show();
                                                     $(".active").hide();
                                            } else {
                                                    $(".inactive").hide();
                                                     $(".active").show();
                                                }
                                                
                                                $(".del").hide();
                                                      jQuery('.recordcount').text(count);
                                                
                                                  } else { 
                                        $(".del").show();
                                        $(".inactive").hide();
                                        $(".active").hide();
                                            }
                                            
                                            jQuery('#stock-id').val(bid);
                                            
                                        });
                                    });
                                </script>
                                <!---- Delete Remodal end ---->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <!--jQuery remove query string parameter from url-->
        <script>
            jQuery(document).ready(function() {
                var uri = window.location.toString();
                if (uri.indexOf("#") > 0) {
                    var clean_uri = uri.substring(0, uri.indexOf("#"));
                    window.history.replaceState({}, document.title, clean_uri);
                }
            });
        </script>
        <!--jQuery remove query string parameter from url-->
        <script>
 jQuery(document).ready(function() {
    jQuery('#example').DataTable( {
        initComplete: function () {
            this.api().columns([0, 1]).every( function () {
                var column = this;
                var select = $('<select><option value="">Select Option</option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
        
    } );
} );
        </script>
        <link rel="stylesheet" type="text/css" href="/wp-content/plugins/market_report/market.css">
        <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel='stylesheet' id='transfers-style-main-css' href='https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css' type='text/css' media='all' />
        <link rel='stylesheet' id='transfers-style-main-css' href='https://cdn.datatables.net/responsive/1.0.7/css/responsive.dataTables.min.css' type='text/css' media='all' />
        <link rel='stylesheet' id='transfers-style-main-css' href='https://cdnjs.cloudflare.com/ajax/libs/remodal/1.0.6/remodal.css' type='text/css' media='all' />
        <link rel='stylesheet' id='transfers-style-main-css' href='https://cdnjs.cloudflare.com/ajax/libs/remodal/1.0.6/remodal-default-theme.min.css' type='text/css' media='all' />


        <script type='text/javascript' src='https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js'></script>
        <script type='text/javascript' src='https://cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js'></script>
        <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/remodal/1.0.6/remodal.min.js'></script>