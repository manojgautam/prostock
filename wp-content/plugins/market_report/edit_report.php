<?php
  global $wpdb;

     $breed_name= $wpdb->prefix .'breeds';
     $breeds = "SELECT * FROM $breed_name WHERE `Active` = 'true'";
      $result =$wpdb->get_results($breeds);
      foreach($result as $data){
                   $Breed[]=$data->Name;
                 }
                //$Breed=array_unique($Breed);
                
                
    $type_name= $wpdb->prefix .'stock_type';
     $Types = "SELECT * FROM $type_name WHERE `Active` = 'true'";
      $result =$wpdb->get_results($Types);
      foreach($result as $data){
                   $Type[]=$data->Name;
                 }
    
 $actual_link = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
 $ids=(explode("id=",$actual_link));
 $id=$ids[1];
          
      $market_table= $wpdb->prefix .'market_report';
        
if( isset( $_POST['update'] ) ) {   
    $lot=$_POST['lot'];
    $type = $_POST['type'];
    $breed = $_POST['breed'];
    $head = $_POST['head'];
    $salemethod = $_POST['salemethod'];
    $perkg = $_POST['perkg'];
    $avgkg = $_POST['avgkg'];
    $avgamount = $_POST['avgamount'];
    $totalweight = $_POST['totalweight'];
    $totalamount = $_POST['totalamount'];
    $buyerid = $_POST['buyerid'];
    $sellerid = $_POST['sellerid'];
    $agentid = $_POST['agentid'];
    $buyercode = $_POST['buyercode'];
    $tempcode = $_POST['tempcode'];
    $saledate = date_format(date_create($_POST['saledate']),'Y-m-d');

$sql = $wpdb->prepare("UPDATE `".$market_table."` SET `Lot` = %d, `Type` = %s, `Breed` = %s, `Head` = %d, `Sale_Method` = %s, `c_kg` = %f, `Avg_kg` = %f, `Avg_Amt` = %f, `Tot_kg` = %f, `Tot_Amt` = %f, `Buyer_ID` = %s, `Seller_ID` = %s, `Agent_ID` = %s, `Buyer_Code` = %s, `Temp_Code` = %s, `Sale_Date` = %s WHERE Id = $id", $lot, $type, $breed, $head, $salemethod, $perkg, $avgkg, $avgamount, $totalweight, $totalamount, $buyerid, $sellerid, $agentid, $buyercode, $tempcode, $saledate );
    
  // echo "<pre>"; print_r($sql);die;
        if( $wpdb->query( $sql ) ) {
         echo "<script>jQuery(document).ready(function(){ jQuery('#setting-error-settings_updated').addClass('updated'); jQuery('#setting-error-settings_updated').removeClass('error'); jQuery('#setting-error-settings_updated').show(); jQuery('#setting-error-settings_updated').find('strong').text('Data Updated Successfully.'); });</script>";
    
    }
} 


     $Types = "SELECT * FROM $market_table WHERE `Id` = $id";
      $report =$wpdb->get_results($Types);
        

?>
<script>
    jQuery(document).ready(function(){
        jQuery(".notice-dismiss").click(function(){
            jQuery(this).parent.hide();
        });
    });
</script>
<script type='text/javascript' src='//code.jquery.com/jquery-2.2.3.min.js'></script>

<div id="wpbody" role="main" class="marketreport">
    <div id="wpbody-content" aria-label="Main content" tabindex="0">
     <div class="wrap nosubsub">
         
         <div id="setting-error-settings_updated" class="updated settings-error notice is-dismissible" style="display:none;"> 
    <p>
        <strong></strong>
    </p>
    <button type="button" class="notice-dismiss">
        <span class="screen-reader-text">Dismiss this notice.</span>
    </button>
</div>

<form method="post" action="" enctype="multipart/form-data" id="addtag" class="validate">
 
  
<div id="col-container" class="wp-clearfix">

  <div id="col-left">
<div class="col-wrap">      
<div class="form-wrap">
	<h1 class="wp-heading-inline">Update Reports</h1>
	 <div class="form-field form-required term-name-wrap">
	<label for="tag-name">Sale Date</label>
	<div id="datepicker" class="input-group date" data-date-format="mm-dd-yyyy">
    <input class="form-control" name="saledate" type="text" value="<?php echo date_format(date_create($report[0]->Sale_Date),'m-d-Y'); ?>" readonly />
    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
</div>
	
   </div>
     <div class="form-field form-required term-name-wrap">
	<label for="tag-name">Lot</label>
	<input name="lot" id="tag-name" type="number" value="<?php echo $report[0]->Lot; ?>" >
   </div>
     <div class="form-field form-required term-name-wrap">
	<label for="tag-name">Stock Type</label>
	<select name="type"><option value="">All</option>    <?php foreach($Type as $data)
                    { ?><option value="<?php echo $data; ?>" <?php if($data == $report[0]->Type){ echo 'selected';} ?> ><?php echo $data; ?></option> <?php } ?></select>
   </div>
    <div class="form-field form-required term-name-wrap">
	<label for="tag-name">Breed</label>
	 <select name="breed"><option value="">All</option>   <?php foreach($Breed as $data)
                    { ?><option value="<?php echo $data; ?>" <?php if($report[0]->Breed == $data){ echo 'selected';} ?> ><?php echo $data; ?></option><?php } ?></select>
   </div>
    <div class="form-field form-required term-name-wrap">
	<label for="tag-name">Head</label>
	<input type="number" name="head" value="<?php echo $report[0]->Head; ?>" >
	</div>
	 <div class="form-field form-required term-name-wrap">
	<label for="tag-name">Sale Method</label>
	<input type="text" name="salemethod" value="<?php echo $report[0]->Sale_Method; ?>" >
	</div>
	 <div class="form-field form-required term-name-wrap">
	<label for="tag-name">Cents Per Kg</label>
	<input type="number" name="perkg" value="<?php echo $report[0]->c_kg; ?>">
	</div>
	 <div class="form-field form-required term-name-wrap">
	<label for="tag-name">Average Kg:</label>
	<input type="number" name="avgkg" value="<?php echo $report[0]->Avg_kg; ?>">
	</div>
</div>
</div>
</div>

<div id="col-right">
<div class="col-wrap">      
<div class="form-wrap">
	<div class="form-field form-required term-name-wrap">
	<label for="tag-name">Average Amount</label>
	<input type="number" name="avgamount" value="<?php echo $report[0]->Avg_Amt; ?>">
	</div>
	 <div class="form-field form-required term-name-wrap">
	<label for="tag-name">Total Weight</label>
	<input type="number" name="totalweight" value="<?php echo $report[0]->Tot_kg; ?>">
	</div>
    <div class="form-field form-required term-name-wrap">
	<label for="tag-name">Total Amount</label>
	<input type="number" name="totalamount" value="<?php echo $report[0]->Tot_Amt; ?>">
	</div>
	<div class="form-field form-required term-name-wrap">
	<label for="tag-name">BuyerID</label>
	<input type="text" name="buyerid" value="<?php echo $report[0]->Buyer_ID; ?>">
	</div>
	<div class="form-field form-required term-name-wrap">
	<label for="tag-name">SellerID</label>
	<input type="text" name="sellerid" value="<?php echo $report[0]->Seller_ID; ?>">
	</div>
	<div class="form-field form-required term-name-wrap">
	<label for="tag-name">AgentID</label>
	<input type="text" name="agentid" value="<?php echo $report[0]->Agent_ID; ?>">
	</div>
	<div class="form-field form-required term-name-wrap">
	<label for="tag-name">Buyer Code</label>
	<input type="text" name="buyercode" value="<?php echo $report[0]->Buyer_Code ; ?>">
	</div>
	<div class="form-field form-required term-name-wrap">
	<label for="tag-name">Temp Code</label>
	<input type="text" name="tempcode" value="<?php echo $report[0]->Temp_Code ; ?>">
	</div>

</div>
</div>
</div>

</div>
<p class="submit"><input type="submit" name="update" id="submit" class="button button-primary" value="Update Report"></p>
</form>
</div>
</div>
</div>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />

<script>
jQuery(function () {
  jQuery("#datepicker").datepicker({ 
        autoclose: true,
        todayHighlight: false
  });
});

</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>