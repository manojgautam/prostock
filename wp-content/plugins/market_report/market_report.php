<?php
/*
Plugin Name: Market Report
Plugin URI: 
Description: Plugin to add and edit the transportation types from admin. Shortcode is [market_data]
Author: tecHindustan
Version: 1.0.0
Author URI:
*/

$siteurl = get_option('siteurl');
define('PLD_FOLDER', dirname(plugin_basename(__FILE__)));
define('PLD_URL', $siteurl.'/wp-content/plugins/' . PLD_FOLDER);
define('PLD_FILE_PATH', dirname(__FILE__));
define('PLD_DIR_NAME', basename(PLD_FILE_PATH));
// this is the table prefix
global $wpdb;
$wp_table_prefix=$wpdb->prefix;
define('LPD_TABLE_PREFIX', $wp_table_prefix);

register_activation_hook(__FILE__,'PLD_install');
register_deactivation_hook(__FILE__ , 'LPD_uninstall' );

function PLD_install()
{
    global $wpdb;
    $table = LPD_TABLE_PREFIX."market_report";    
    $structure = "CREATE TABLE $table (
        Id INT(11) NOT NULL AUTO_INCREMENT,
        Lot int(11) NOT NULL,
        Type varchar(50) NOT NULL,
        Breed varchar(50) NOT NULL,
        Head int(11) NOT NULL,
        Sale_Method varchar(51) NOT NULL,
        c_kg float NOT NULL,
        Avg_kg float NOT NULL,
        Avg_Amt float NOT NULL,
        Tot_kg float NOT NULL,
        Tot_Amt float NOT NULL,
        Buyer_ID varchar(50) NOT NULL,
        Seller_ID varchar(50) NOT NULL,
        Agent_ID varchar(50) NOT NULL,
        Buyer_Code varchar(50) NOT NULL,
        Temp_Code varchar(50) NOT NULL,
        Sale_Date date NOT NULL,
        create_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        UNIQUE KEY Id (Id)
    )";
    $wpdb->query($structure);       // Execute query    
    
     $table = LPD_TABLE_PREFIX."breeds";    
    $structure1 = "CREATE TABLE $table (
        Id INT(11) NOT NULL AUTO_INCREMENT,
        Name varchar(50) NOT NULL,
        Active varchar(10) NOT NULL,
        create_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        UNIQUE KEY Id (Id)
    )";
    $wpdb->query($structure1);       // Execute query  
    
   $table = LPD_TABLE_PREFIX."stock_type";    
    $type = "CREATE TABLE $table (
        Id INT(11) NOT NULL AUTO_INCREMENT,
        Name varchar(50) NOT NULL,
        Active varchar(10) NOT NULL,
        create_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        UNIQUE KEY Id (Id)
    )";
    $wpdb->query($type);       // Execute query  
    
       $table = LPD_TABLE_PREFIX."sale_units";    
    $units = "CREATE TABLE $table (
        Id INT(11) NOT NULL AUTO_INCREMENT,
        Name varchar(50) NOT NULL,
        Active varchar(10) NOT NULL,
        create_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        UNIQUE KEY Id (Id)
    )";
    $wpdb->query($units);       // Execute query  
    wp_reset_query();   // Reset wordpress query

}
function LPD_uninstall()
{
    global $wpdb;
    
}

add_action('admin_menu','lpd_menu');    // Admin menu hook

/*  Function is used to add a new menu in plugin    */
function lpd_menu()     
{ 
    /*add_menu_page(
        "Market Report",
        "Market Report",
        8,
        __FILE__,
        "market_reports",
        PLD_URL."/blaze.png"
    );  // Create a main menu*/
    add_menu_page(
        __('Market Report'),// the page title
        __('Market Report'),//menu title
        'edit_posts',//capability 
        'market-reports',//menu slug/handle this is what you need!!!
        'market_reports_data',//callback function
        '',//icon_url,
        ''//position
    );
    // add_submenu_page(
    //     'market-reports',
    //     'Add Report', //page title
    //     'Add Report', //menu title
    //     'edit_themes', //capability,
    //     'market-reports',//menu slug
    //     'market_reports' //callback function
    // );
    add_submenu_page(
        'market-reports',
        'Report Data', //page title
        'All data', //menu title
        'edit_posts', //capability,
        'Alldata',//menu slug
        'market_reports_data' //callback function
    );
    add_submenu_page(
        'market-reports',
        'Breeds', //page title
        'Breeds', //menu title
        'edit_posts', //capability,
        'breeds',//menu slug
        'breeds' //callback function
    );
        add_submenu_page(
        'market-reports',
        'Stock Types', //page title
        'Stock Types', //menu title
        'edit_posts', //capability,
        'stock_type',//menu slug
        'stock_type' //callback function
    );
         add_submenu_page(
        'market-reports',
        'Sale Units', //page title
        'Sale Units', //menu title
        'edit_posts', //capability,
        'sale_units',//menu slug
        'sale_units' //callback function
    );

}
/*----------Breeds function---------------*/
function breeds(){
    include_once( plugin_dir_path( __FILE__ ) . 'breeds.php' );
}
/*----------Breeds function end---------------*/

/*----------Stock Type function---------------*/
function stock_type(){
    include_once( plugin_dir_path( __FILE__ ) . 'stock_type.php' );
}
/*----------Stock Type function end---------------*/

/*----------Sale Units function---------------*/
function sale_units(){
    include_once( plugin_dir_path( __FILE__ ) . 'sale_unit.php' );
}
/*----------Sale Units function end---------------*/
// function market_reports()
// {
//   include_once( plugin_dir_path( __FILE__ ) . 'add_report.php' );
// }

function market_reports_data(){
    global $wpdb;
     $table_name= $wpdb->prefix .'market_report';
     
    if( isset( $_POST['delete'] ) ) {  
    $id= $_POST['reportid'];
$del = $wpdb->query(
              'DELETE  FROM '.$table_name.'
               WHERE Id = "'.$id.'"'
);
 echo "<script>jQuery(document).ready(function(){ jQuery('#setting-error-settings_updated').addClass('updated'); jQuery('#setting-error-settings_updated').removeClass('error'); jQuery('#setting-error-settings_updated').show(); jQuery('#setting-error-settings_updated').find('strong').text('Record Deleted.'); });</script>";
    
}

//update admin report data by id

$market_table = $wpdb->prefix."market_report";

$actual_link = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
$ids=(explode("edit_",$actual_link));
$id=$ids[1];

if( isset( $_POST['edit'] ) ) { 
  $id=$_POST['id'];  
  $lot=$_POST['lot'];
  $type = $_POST['type'];
  $breed = $_POST['breed'];
  $head = $_POST['head'];
  $salemethod = $_POST['salemethod'];
  $perkg = $_POST['perkg'];
  $avgkg = $_POST['avgkg'];
  $avgamount = $_POST['avgamount'];
  $totalweight = $_POST['totalweight'];
  $totalamount = $_POST['totalamount'];
  $buyerid = $_POST['buyerid'];
  $sellerid = $_POST['sellerid'];
  $agentid = $_POST['agentid'];
  $buyercode = $_POST['buyercode'];
  $tempcode = $_POST['tempcode'];
  $saledate = DateTime::createFromFormat('m-d-Y', $_POST['saledate'])->format('Y-m-d');

$sql = $wpdb->prepare("UPDATE `".$market_table."` SET `Lot` = %d, `Type` = %s, `Breed` = %s, `Head` = %d, `Sale_Method` = %s, `c_kg` = %f, `Avg_kg` = %f, `Avg_Amt` = %f, `Tot_kg` = %f, `Tot_Amt` = %f, `Buyer_ID` = %s, `Seller_ID` = %s, `Agent_ID` = %s, `Buyer_Code` = %s, `Temp_Code` = %s, `Sale_Date` = %s WHERE Id = $id", $lot, $type, $breed, $head, $salemethod, $perkg, $avgkg, $avgamount, $totalweight, $totalamount, $buyerid, $sellerid, $agentid, $buyercode, $tempcode, $saledate );
  
 //echo "<pre>"; print_r($sql);die;
      if( $wpdb->query( $sql ) ) {
       echo "<script>jQuery(document).ready(function(){ jQuery('#setting-error-settings_updated').addClass('updated'); jQuery('#setting-error-settings_updated').removeClass('error'); jQuery('#setting-error-settings_updated').show(); jQuery('#setting-error-settings_updated').find('strong').text('Data Updated Successfully.'); });</script>";
  
  }
}


//end of update report data
    
  // Select Query For queryStringshow result a wp admin all data 

  $urlcount = $_GET['start']; 

// serach code
  $searchQuery="";
  $queryString="/wp-admin/admin.php?page=Alldata";
  if($_GET['submitreport']){
    $submitreport=$_GET['submitreport'];
  

      $s_sale_from_admin = ($_GET['salefrom']?date_format(date_create($_GET['salefrom']),'Y-m-d'):'');
      $s_sale_to_admin = ($_GET['saleto']?date_format(date_create($_GET['saleto']),'Y-m-d'):'');
      $s_breed_admin = $_GET['breed'];
      $s_type_admin = $_GET['type'];
      $query = "";

      if($s_sale_from_admin || $s_sale_to_admin || $s_breed_admin || $s_type_admin){
      $query .="WHERE ";
      
                    if($s_sale_from_admin){
                       $query .= " Sale_Date >= '" .$s_sale_from_admin."' AND";
                    }
                    if($s_sale_to_admin){
                       $query .= " Sale_Date <= '" .$s_sale_to_admin."' AND";
                    }

                    if($s_breed_admin){
                       $query .= " Breed = '" .$s_breed_admin."' AND";
                    }
                    if($s_type_admin){
                       $query .= " Type = '" .$s_type_admin."' AND";
                    }
       // remove last AND from query
       $searchQuery=substr($query,0,-3);

   }

    $queryString=$_SERVER['REQUEST_URI'];
}
// serach code
  $startlimit = 0;
  $per_page = 50;
     if(isset($urlcount)){
        $startlimit = $urlcount;
     } 
      $urlpageno = ($startlimit/$per_page)+1;
      
       $sql2="SELECT * FROM $table_name $searchQuery ORDER BY Sale_Date DESC, Lot ASC LIMIT $startlimit, $per_page";
                  $resuth1=$wpdb->get_results($sql2);
        

// Select Query For pagination
                  $sqlpagi="SELECT count(Id) as id FROM $table_name $searchQuery ORDER BY Sale_Date DESC, Lot ASC";
                  $resutpage=$wpdb->get_results($sqlpagi);
                  $rowcount= $resutpage[0]->id;
                   // $rowcount= $wpdb->num_rows;
                //    $rowcount=55000;
                 $totalpages = (float)$rowcount/$per_page;
                  $totalpages = floor($totalpages);
                 
                 
                      $table_name= $wpdb->prefix .'market_report';
if(isset($_POST['submit']))
{
    $fileToUpload=$_FILES["fileToUpload"];
    if (($_FILES["fileToUpload"]["type"] == "text/csv"))
        {
            $file = $_FILES['fileToUpload']['tmp_name']; 
            $handle = fopen($file,"r"); 
            $i=1;
            
            do 
            {
                if ($data[0]) 
                    {

                        if($i!=1){
                        //     echo "<pre>";
                        // print_r($data);die;
                                    $query = $wpdb->insert( $table_name, array(
                                    'Lot' =>addslashes($data[1]),
                                    'Type' =>addslashes($data[2]),
                                    'Breed' =>addslashes($data[3]),
                                    'Head' =>addslashes($data[4]),
                                    'Sale_Method'=>addslashes($data[5]),
                                    'c_kg' =>addslashes($data[6]),
                                    'Avg_kg' =>addslashes($data[7]),
                                    'Avg_Amt' =>addslashes($data[8]),
                                    'Tot_kg' =>addslashes($data[9]),
                                    'Tot_Amt' =>addslashes($data[10]),
                                    'Buyer_ID' =>addslashes($data[11]),
                                    'Seller_ID' =>addslashes($data[12]),
                                    'Agent_ID' =>addslashes($data[13]),
                                    'Buyer_Code' =>addslashes($data[14]),
                                    'Temp_Code' =>addslashes($data[15]),
                                    'Sale_Date' =>addslashes(date_format(date_create($data[0]),'Y-m-d'))
                                    ));


                              // add breed in breed table
                              $breed_table= $wpdb->prefix .'breeds';
                              $breeds = "SELECT * FROM $breed_table WHERE `Name` = '".addslashes($data[3])."'";
                              $result1 =$wpdb->get_results($breeds);
                              if(!count($result1)){
                             
                                        $wpdb->insert( $breed_table, array(
                                                  'Name' =>addslashes($data[3]),
                                                  'Active' =>'true'
                                        ));
                              }
                                   
                              $type_table= $wpdb->prefix .'stock_type';
                              $Types = "SELECT * FROM $type_table WHERE `Name` = '".addslashes($data[2])."'";
                              $result2 =$wpdb->get_results($Types);
                                if(!count($result2)){
                                        
                                        $wpdb->insert( $type_table, array(
                                                  'Name' =>addslashes($data[2]),
                                                  'Active' =>'true'
                                        ));
                              }     

                                }
                        $i++;
                    }
            } 
            while ($data = fgetcsv($handle,1000,",","'") ); 
            echo "<script>jQuery(document).ready(function(){ jQuery('#setting-error-settings_updated').addClass('updated'); jQuery('#setting-error-settings_updated').removeClass('error'); jQuery('#setting-error-settings_updated').show(); jQuery('#setting-error-settings_updated').find('strong').text('Data imported.'); });</script>";
    
        } else {

            echo "<script>$(document).ready(function(){ $('#setting-error-settings_updated').addClass('error'); $('#setting-error-settings_updated').removeClass('updated'); $('#setting-error-settings_updated').show(); $('#setting-error-settings_updated').find('strong').text('Please Upload the CSV file'); });</script>";
            
        }
}

?>
<script>
    jQuery(document).ready(function(){
        jQuery(".notice-dismiss").click(function(){
            jQuery(this).parent.hide();
        });
    });
</script>
<div id="wpbody" role="main">

<div id="wpbody-content" aria-label="Main content" tabindex="0">
        
<div class="wrap">
    <div id="setting-error-settings_updated" class="updated settings-error notice is-dismissible" style="display:none;"> 
        <p>
            <strong></strong>
        </p>
        <button type="button" class="notice-dismiss">
            <span class="screen-reader-text">Dismiss this notice.</span>
        </button>
    </div>
    
<div class="admin-form col-md-12">
    <h1>Import Stock Report</h1>
    <p>Note: Your CSV must have a header row and the following column order:<br/>
    Sale Date | Lot | Stock Type | Breed | Head | Sale Method | c per kg | AvgKg | AvgAmt | TotWgt | TotAmt | Buyer ID | Seller ID | Agent ID | Buyer Code | Temp Code</p>

  <div class="row uploadcsv">
   <form method="post" action="" enctype="multipart/form-data">

    <table class="form-table">
        <tbody>
            <tr>
                <th scope="row"><label for="blazeAPIKey">Please upload CSV file here</label></th>
                <td>
                    <input type="file" name="fileToUpload" accept=".csv" class="regular-text" required/>
                </td>
            </tr>
        </tbody>
    </table>
    <p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Upload"></p>
   </form>
</div>

</div>
<div class="col-md-12">
<div class="row">
  
<!-- filter form - Admin -->
      <form action="" method="get">
 
                   <?php  $user = wp_get_current_user();
                          $allowed_roles = array('public', 'administrator');

                              global $wpdb;

                              $breed_name= $wpdb->prefix .'breeds';
                              $breeds = "SELECT * FROM $breed_name WHERE `Active` = 'true' ORDER BY  `Name` ";
                              $result =$wpdb->get_results($breeds);
                              foreach($result as $data){
                                           $Breedadmin[]=$data->Name;
                                         }
                                   
                              $type_name= $wpdb->prefix .'stock_type';
                              $Types = "SELECT * FROM $type_name WHERE `Active` = 'true' ORDER BY  `Name`";
                              $result =$wpdb->get_results($Types);
                              foreach($result as $data){
                                           $Typeadmin[]=$data->Name;
                                         } 

                        ?>
              
              <div class="search_block_admin row">
                  <div class="col-sm-3"> 
                  <div class="form-group"> 
          <label>Breed: </label><select name="breed"><option value="">All</option>   <?php foreach($Breedadmin as $data)
                    { ?><option value="<?php echo $data; ?>" <?php if(isset($s_breed_admin) && $s_breed_admin == $data){ echo 'selected';} ?> ><?php echo $data; ?></option><?php } ?></select>
                    </div>
                    </div>
                    <div class="col-sm-3">
                    <div class="form-group">
          <label>Type: </label><select name="type"><option value="">All</option>    <?php foreach($Typeadmin as $data)
                    { ?><option value="<?php echo $data; ?>" <?php if(isset($s_type_admin) && $s_type_admin == $data){ echo 'selected';} ?> ><?php echo $data; ?></option> <?php } ?></select>
          </div>
          </div>
                    <div class="col-sm-3"> 
                    <div class="form-group"> 
          <label>Sale Date From: </label>
                    <div id="datepickerfrom" class="input-group date" data-date-format="dd MM yyyy">
                        <input class="form-control" type="text" name="salefrom" value="<?php if(isset($s_sale_from_admin) && $s_sale_from_admin!=''){echo date_format(date_create($s_sale_from_admin),'d F Y');}else{echo '';}?>" readonly />
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                    </div>
          </div>
          </div>
                        <div class="col-sm-3"> 
                        <div class="form-group"> 
            <label>To: </label>
                    <div id="datepickerto" class="input-group date" data-date-format="dd MM yyyy">
                        <input class="form-control" type="text" name="saleto" value="<?php if(isset($s_sale_to_admin) && $s_sale_to_admin!=''){echo date_format(date_create($s_sale_to_admin),'d F Y');}else{echo '';} ?>" readonly />
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                    </div>
          </div>
          </div>    <input type="hidden" name="page" value="Alldata" />
                    <input type="submit" name="submitreport" id="submitreport" value="Search" class="btn btn-primary">
          </div>


                    </form>
</div>
</div>





<h1> Market Report Data</h1>
<table id="" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Sale Date</th>
                <th>Type</th>
                <th>Breed</th>
                <th>Sale Method</th>
                <th class="right">Lot</th>
                <th class="right">Head</th>
                <th class="right">c/kg</th>
                <th class="right">Avg kg</th>
                <th class="right">Avg Amt</th>
                <th class="right">Tot kg</th>
                <th class="right">Tot Amt</th>
                <th>Action</th>
                
            </tr>
        </thead>
       
        <tbody>
        <?php 
        
        if(!count($resuth1)){echo '<tr class="odd"><td valign="top" colspan="13" class="dataTables_empty">No matching records found</td></tr>';}
       foreach($resuth1 as $data){
            echo "<tr>";
            echo "<td>".date_format(date_create($data->Sale_Date),'d M Y')."</td>";
            //echo "<td>".$data->Sale_Date."</td>";
            echo "<td>".$data->Type."</td>";
            echo "<td>".$data->Breed."</td>";
            echo "<td>".$data->Sale_Method."</td>";
            echo "<td class='right'>".$data->Lot."</td>";
            echo "<td class='right'>".$data->Head."</td>";
            echo "<td class='right'>".$data->c_kg."</td>";          
            echo "<td class='right'>".number_format($data->Avg_kg,1)."</td>";
            echo "<td class='right'>".number_format($data->Avg_Amt,2)."</td>";
            echo "<td class='right'>".number_format($data->Tot_kg,1)."</td>";
            echo "<td class='right'>".number_format($data->Tot_Amt,2)."</td>";
            
            echo "<td><span class='edit'><a href='#edit_$data->Id'>Edit</a> |</span> <span class='trash'><a href='#del' class='report-del' attr-id='$data->Id'>Delete</a><span></td>";
            echo "</tr>";  ?>

            <!----Edit  Remodal---->
               <div class="remodal" data-remodal-id="edit_<?php echo $data->Id;?>" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
             <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
             <form method="post" action="" enctype="multipart/form-data" id="addtag" class="validate">

             <div id="col-container" class="wp-clearfix">

               <div id="col-left" class="reports_block">
             <div class="col-wrap">      
             <div class="form-wrap">
                <h1 class="wp-heading-inline">Edit Report</h1>

                 <div class="form-field form-required term-name-wrap">
                <label for="tag-name">Sale Date</label>
                
                    <div id="datepicker" class="input-group date" data-date-format="mm-dd-yyyy">
                 <input class="form-control" name="saledate" type="text" value="<?php  echo  DateTime::createFromFormat('Y-m-d', $data->Sale_Date)->format('m-d-Y'); ?>" readonly />
                 <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
             </div>
                </div>
                
                  <div class="form-field form-required term-name-wrap">
                <label for="tag-name">Lot</label>
                <input name="lot" id="tag-name" type="number" value="<?php echo $data->Lot; ?>" required/>
                <input type="hidden" name="id" value="<?php echo $data->Id;?>">
                </div>
                  <div class="form-field form-required term-name-wrap">
                <label for="tag-name">Stock Type</label>
                <select name="type"><option value="">All</option>    <?php foreach($Typeadmin as $type)
                                 { ?><option value="<?php echo $type; ?>" <?php if($type == $data->Type){ echo 'selected';} ?> ><?php echo $type; ?></option> <?php } ?></select>
                </div>
                 <div class="form-field form-required term-name-wrap">
                <label for="tag-name">Breed</label>
                 <select name="breed"><option value="">All</option>   <?php foreach($Breedadmin as $breed)
                                 { ?><option value="<?php echo $breed; ?>" <?php if($data->Breed == $breed){ echo 'selected';} ?>><?php echo $breed; ?></option><?php } ?></select>
                </div>
                 <div class="form-field form-required term-name-wrap">
                <label for="tag-name">Head</label>
                <input type="number" name="head" value="<?php echo $data->Head; ?>" required/>
                </div>
                 <div class="form-field form-required term-name-wrap">
                <label for="tag-name">Sale Method</label>
                <input type="text" name="salemethod" value="<?php echo $data->Sale_Method; ?>" required/>
                </div>
                 <div class="form-field form-required term-name-wrap">
                <label for="tag-name">Cents Per Kg</label>
                <input type="number" name="perkg" value="<?php echo $data->c_kg; ?>">
                </div>
                 <div class="form-field form-required term-name-wrap">
                <label for="tag-name">Average Kg:</label>
                <input type="number" name="avgkg" value="<?php echo $data->Avg_kg; ?>">
                </div>
             </div>
             </div>
             </div>

             <div id="col-right" class="reports_block_right">
             <div class="col-wrap">      
             <div class="form-wrap">
                <div class="form-field form-required term-name-wrap">
                <label for="tag-name">Average Amount</label>
                <input type="number" name="avgamount" value="<?php echo $data->Avg_Amt; ?>">
                </div>
                 <div class="form-field form-required term-name-wrap">
                <label for="tag-name">Total Weight</label>
                <input type="number" name="totalweight" value="<?php echo $data->Tot_kg; ?>">
                </div>
                 <div class="form-field form-required term-name-wrap">
                <label for="tag-name">Total Amount</label>
                <input type="number" name="totalamount" value="<?php echo $data->Tot_Amt; ?>">
                </div>
                <div class="form-field form-required term-name-wrap">
                <label for="tag-name">BuyerID</label>
                <input type="text" name="buyerid" value="<?php echo $data->Buyer_ID;?>">
                </div>
                <div class="form-field form-required term-name-wrap">
                <label for="tag-name">SellerID</label>
                <input type="text" name="sellerid" value="<?php echo $data->Seller_ID; ?>">
                </div>
                <div class="form-field form-required term-name-wrap">
                <label for="tag-name">AgentID</label>
                <input type="text" name="agentid" value="<?php echo $data->Agent_ID;  ?>">
                </div>
                <div class="form-field form-required term-name-wrap">
                <label for="tag-name">Buyer Code</label>
                <input type="text" name="buyercode" value="<?php echo $data->Buyer_Code; ?>">
                </div>
                <div class="form-field form-required term-name-wrap">
                <label for="tag-name">Temp Code</label>
                <input type="text" name="tempcode" value="<?php echo $data->Temp_Code; ?>">
                </div>

             </div>
             </div>
             </div>

             </div>
             <p class="submit"><input type="submit" name="edit" id="submit" class="button button-primary" value="Update Report"></p>
             </form>
    
             </div>
                     <!----Edit  Remodal end---->
        <?php    }
        ?>
        </tbody>
    </table>

    <!-- Create the query -->
       <?php 
       function get_paging_info($tot_rows,$pp,$curr_page)
       {
           $pages = ceil($tot_rows / $pp); // calc pages

           $data = array(); // start out array
           $data['si']        = ($curr_page * $pp) - $pp; // what row to start at
           $data['pages']     = $pages;                   // add the pages
           $data['curr_page'] = $curr_page;               // Whats the current page

           return $data; //return the paging data

       }
       ?>
       <?php $count = $rowcount; ?>

   <!-- Call our function from above -->
   <?php $paging_info = get_paging_info($count,$per_page,$urlpageno); ?>


   <div class="pagination_report">
       <!-- If the current page is more than 1, show the First and Previous links -->
       <?php if($paging_info['curr_page'] > 1) : ?>
           <a href='<?php echo $queryString;?>&start=<?php echo 0;?>' title='Page 1'>First</a>
           <a href='<?php echo $queryString;?>&start=<?php echo $per_page*($paging_info['curr_page']-2); ?>'  title='Page <?php echo ($paging_info['curr_page'] - 1); ?>'>Prev</a>
       <?php endif; ?>



       <?php
           //setup starting point

           //$max is equal to number of links shown
           $max = 7;
           if($paging_info['curr_page'] < $max)
               $sp = 1;
           elseif($paging_info['curr_page'] >= ($paging_info['pages'] - floor($max / 2)) )
               $sp = $paging_info['pages'] - $max + 1;
           elseif($paging_info['curr_page'] >= $max)
               $sp = $paging_info['curr_page']  - floor($max/2);
       ?>

       <!-- If the current page >= $max then show link to 1st page -->
       <?php if($paging_info['curr_page'] >= $max) : ?>

           <a href='<?php echo $queryString;?>&start=<?php echo 0; ?>' title='Page 1'>1</a>
           ..

       <?php endif; ?>

       <!-- Loop though max number of pages shown and show links either side equal to $max / 2 -->
       <?php for($i = $sp; $i <= ($sp + $max -1);$i++) : ?>

           <?php
               if($i > $paging_info['pages'])
                   continue;
           ?>

           <?php if($paging_info['curr_page'] == $i) : ?>

               <span class='bold'><?php echo $i; ?></span>
               

           <?php else : ?>

               <a href='<?php echo $queryString;?>&start=<?php echo $per_page*($i-1); ?>' title='Page <?php echo $i; ?>'><?php echo $i; ?></a>

           <?php endif; ?>

       <?php endfor; ?>


       <!-- If the current page is less than say the last page minus $max pages divided by 2-->
       <?php if($paging_info['curr_page'] < ($paging_info['pages'] - floor($max / 2))) : ?>

           ..
           <a href='<?php echo $queryString;?>&start=<?php echo $per_page*($paging_info['pages']-1); ?>' title='Page <?php echo $paging_info['pages']; ?>'><?php echo $paging_info['pages']; ?></a>

       <?php endif; ?>

       <!-- Show last two pages if we're not near them -->
       <?php if($paging_info['curr_page'] < $paging_info['pages']) : ?>

           <a href='<?php echo $queryString;?>&start=<?php echo $per_page*$paging_info['curr_page']; ?>'  title='Page <?php echo ($paging_info['curr_page'] + 1); ?>'>Next</a>

           <a href='<?php echo $queryString;?>&start=<?php echo $per_page*($paging_info['pages']-1); ?>' title='Page <?php echo $paging_info['pages']; ?>'>Last</a>

       <?php endif; ?>
   </div>



    
 <!----Delete Remodal---->
    <div class="remodal" data-remodal-id="del" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
  <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
  <div>
    <h2 id="modal1Title">Are you sure?</h2>
<form method="post" action="" enctype="multipart/form-data">
    <div class="form-field form-required term-name-wrap">
    <input type="hidden" name="reportid" id="report-id" value="">
</div>

 <p id="modal1Desc">
     You will not be able to recover this Report
    </p>
  </div>
  <br>
  <button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
  <input type="submit" name="delete" id="submit" class="remodal-confirm" value="Yes, delete it!">
</form>
  </div>

</div>
<script>
  jQuery(document).ready(function() {
      jQuery('.report-del').click(function(){
      var rid =  $(this).attr('attr-id');
      jQuery('#report-id').val(rid);
      });
  });
</script>
<!---- Delete Remodal end ---->
    </div>
    </div>

<!--jQuery remove query string parameter from url-->
<script>
    jQuery(document).ready(function(){
    var uri = window.location.toString();
    if (uri.indexOf("#") > 0) {
        var clean_uri = uri.substring(0, uri.indexOf("#"));
        window.history.replaceState({}, document.title, clean_uri);
    }
});
</script>

<script>
jQuery(function () {
  jQuery(".date").datepicker({ 
        autoclose: true,
        defaultDate: '',
        setDate:'',
         autoUpdateInput: false,
        todayHighlight: false
  });
 
});

</script>
<!--jQuery remove query string parameter from url-->
 
    <link rel="stylesheet" type="text/css" href="/wp-content/plugins/market_report/market.css">
<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel='stylesheet' id='transfers-style-main-css'  href='https://cdnjs.cloudflare.com/ajax/libs/remodal/1.0.6/remodal.css' type='text/css' media='all' />
<link rel='stylesheet' id='transfers-style-main-css'  href='https://cdnjs.cloudflare.com/ajax/libs/remodal/1.0.6/remodal-default-theme.min.css' type='text/css' media='all' />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/remodal/1.0.6/remodal.min.js'></script>
<?php
}




/* front end*/



function market_data_front(){
    global $wpdb;

 $breed_name= $wpdb->prefix .'breeds';
 $breeds = "SELECT * FROM $breed_name WHERE `Active` = 'true'";
  $result =$wpdb->get_results($breeds);
  foreach($result as $data){
               $Breed[]=$data->Name;
             }
            //$Breed=array_unique($Breed);
            
            
$type_name= $wpdb->prefix .'stock_type';
 $Types = "SELECT * FROM $type_name WHERE `Active` = 'true'";
  $result =$wpdb->get_results($Types);
  foreach($result as $data){
               $Type[]=$data->Name;
             }
            
 $table_name= $wpdb->prefix .'market_report';
// $sql2="SELECT * FROM $table_name LIMIT 100";
//            $resuth1=$wpdb->get_results($sql2);
//            $resuth2= $wpdb->num_rows;
          /*   foreach($resuth1 as $data){
               $Type[]=$data->Type;
             }
            $Type=array_unique($Type);*/
            //print_r($Breed);die;

            // new code start

            $urlcount = $_GET['start']; 
            // serach code
              $searchQuery="";
              $queryString="/market-reports?";
              $startlimit = 0;
              $per_page = 50;
                 if(isset($urlcount)){
                    $startlimit = $urlcount;
                 } 
                  $urlpageno = ($startlimit/$per_page)+1;
              if($_GET['submitreport']){
                $submitreport=$_GET['submitreport'];
              
            
               $s_sale_from = date_format(date_create($_GET['salefrom']),'Y-m-d');
               $s_sale_to = date_format(date_create($_GET['saleto']),'Y-m-d');
               $s_breed = $_GET['breed'];
               $s_type = $_GET['type'];
               $query = "WHERE ";
              
               if($s_sale_from){
                $query .= " Sale_Date >= '" .$s_sale_from."' AND";
             }
             if($s_sale_to){
                $query .= " Sale_Date <= '" .$s_sale_to."' AND";
             }
               if($s_breed){
                  $query .= " Breed = '" .$s_breed."' AND";
               }
               if($s_type){
                  $query .= " Type = '" .$s_type."' AND";
               }
               //start working of filtering based on radiobutton

              if ( is_user_logged_in() ) {
                    $userid = get_current_user_id();
                  $buyerid= get_user_meta( $userid, 'buyer_id', true ); 
                  if($buyerid ==''){
                    $buyerid='000';
                  }
                  $sellerid= get_user_meta( $userid, 'seller_id', true ); 
                  if($sellerid ==''){
                    $sellerid='000';
                  }
                  $agentid= get_user_meta( $userid, 'agent_id', true ); 
                  if($agentid ==''){
                    $agentid='000';
                  }
              }

           
               
               $orderby=" order by  Sale_Date DESC,Lot ASC ";
                                   $radioitem = $_GET['items'];
                                   if($radioitem == 0){
                                       $query .= "";
                                   }
                                   if($radioitem == 1){
                                       $query .= " Buyer_ID = '" .$buyerid."' AND";
                                   }
                                   if($radioitem == 2){
                                       $query .= " Seller_ID = '" .$sellerid."' AND";
                                   }
                                   if($radioitem == 3){
                                     $orderby=" ORDER BY Seller_ID, Sale_Date DESC ";
                                       $query .= " Agent_ID = '" .$agentid."' AND";
                                   }
                                   if($radioitem == 4){
                                       $orderby=" ORDER BY Seller_ID, Sale_Date DESC "; 
                                       $query .= " Agent_ID = '" .$agentid."' AND";
                                   }
                                   if($radioitem == 5){
                                       $orderby=" ORDER BY Buyer_ID, Buyer_Code ASC "; 
                                       $query .= " Agent_ID = '" .$agentid."' AND";
                                   }
                  // remove last AND from query
                  $searchQuery=substr($query,0,-3);
                  $searchQuery=$searchQuery.$orderby;
                  $queryString=$_SERVER['REQUEST_URI'].'&';

           
        
                  $sql2="SELECT * FROM $table_name $searchQuery  LIMIT $startlimit, $per_page";
                  //echo $sql2;die;
                  $Currentweeksale=$wpdb->get_results($sql2);
  
                             $sqlpagi="SELECT count(Id) as id FROM $table_name $searchQuery";
                             $resutpage=$wpdb->get_results($sqlpagi);
                             $rowcount= $resutpage[0]->id;
                              // $rowcount= $wpdb->num_rows;
                           //    $rowcount=55000;
                            $totalpages = (float)$rowcount/$per_page;
                             $totalpages = floor($totalpages);


                             $totalamt="SELECT sum(Tot_Amt) as totalamt FROM $table_name $searchQuery";
                           $totalamt=$wpdb->get_results($totalamt);
                           $head1="SELECT sum(Head) as head FROM $table_name $searchQuery";
                           $head1=$wpdb->get_results($head1);
                           $headtotal= $head1[0]->head;
                           $totalamt= $totalamt[0]->totalamt;
                           
            } else {

                $sql2="SELECT * FROM $table_name WHERE Sale_Date = (SELECT Sale_Date FROM $table_name ORDER BY Sale_Date DESC LIMIT 1 ) order by Lot ASC LIMIT $startlimit, $per_page";
                $Currentweeksale=$wpdb->get_results($sql2);

                           $sqlpagi="SELECT count(Id) as id FROM $table_name WHERE Sale_Date = (SELECT Sale_Date FROM $table_name ORDER BY Sale_Date DESC LIMIT 1 )";

                           $resutpage=$wpdb->get_results($sqlpagi);
                           $rowcount= $resutpage[0]->id;
                            // $rowcount= $wpdb->num_rows;
                         //    $rowcount=55000;
                          $totalpages = (float)$rowcount/$per_page;
                           $totalpages = floor($totalpages);

            }

function get_paging_info($tot_rows,$pp,$curr_page)
       {
           $pages = ceil($tot_rows / $pp); // calc pages

           $data = array(); // start out array
           $data['si']        = ($curr_page * $pp) - $pp; // what row to start at
           $data['pages']     = $pages;                   // add the pages
           $data['curr_page'] = $curr_page;               // Whats the current page

           return $data; //return the paging data

       } 

$paging_info = get_paging_info($rowcount,$per_page,$urlpageno);
//echo "<pre>";print_r($paging_info);echo "hello";
/* CSV CODE STARTED*/
$filename='usersData';
     $result=$Currentweeksale;
     $a=array();
     $arr=array();
     $arr[]=array("Sale Date", "Lot", "Type", "Breed", "Head", "Sale Method", "c/kg", "Avg kg", "Avg Amt", "Tot kg", "Tot Amt", "Created Date");

     foreach($result as $key=>$user){
            $arr[]=array($user->Sale_Date,$user->Lot,$user->Type,$user->Breed,$user->Head,$user->Sale_Method,$user->c_kg,$user->Avg_kg,$user->Avg_Amt,$user->Tot_kg,$user->Tot_Amt,$user->create_at);
         
     }


     $dir = plugin_dir_path( __FILE__ );
      $filepath =$dir.'/csv/stock_report.csv'; 
      $fp = fopen($filepath, 'w+');
       foreach ( $arr as $line ) { fputcsv($fp, $line); } fclose($fp);

            
            ?>
            <html>
            <head>
              <meta name="viewport" content="width=device-width, initial-scale=1">
              <link rel="stylesheet" type="text/css" href="/wp-content/plugins/market_report/market.css">
              <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />

<link rel='stylesheet' id='transfers-style-main-css'  href='https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css' type='text/css' media='all' />
<link rel='stylesheet' id='transfers-style-main-css'  href='https://cdn.datatables.net/responsive/1.0.7/css/responsive.dataTables.min.css' type='text/css' media='all' />

<script>
jQuery(function () {
jQuery(".date").datepicker({ 
    autoclose: true,
    todayHighlight: false
});

});

</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script type='text/javascript' src='https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js'></script>
<script type='text/javascript' src='https://cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js'></script>

<script>
jQuery(document).ready(function() {
    jQuery('#marketreport').dataTable( {
         "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
          "iDisplayLength": 50,
          "responsive": true
});
} );
</script>  
            </head>
            <body>

            <div class="container">                                                                                      
            <div class="row">                                                                                      
              <div class="table-data">  
              <div class="options_reports">
                  <form action="" method="get">
                  
                      <div class="admin-radio">
                     
                      
               <?php 
//get current user buyer, seller,agent id
                $user = wp_get_current_user();
                 
  //get current user buyer, seller,agent id
                    $allowed_roles = array('public', 'administrator');
                    if( array_intersect($allowed_roles, $user->roles ) ) {  ?> 
                      
                      <ul>
                      <li> 
                      <input id="all" type="radio" name="items" value="0" <?php if(isset($radioitem) && $radioitem  == 0){ echo 'checked';} ?>>
                        <label for="all">All</label>
                       </li>
                       <li> 
                      <input id="purchase" type="radio" name="items" value="1" <?php if(isset($radioitem) && $radioitem  == 1){ echo 'checked';} ?>>
                        <label for="purchase">Purchases</label>
                       </li>
                      <li>
                      <input id="sales" type="radio" name="items" value="2" <?php if(isset($radioitem) && $radioitem  == 2){ echo 'checked';} ?>>
                      <label for="sales">Sales</label>
                      </li>
                      <li>
                      <input id="bookingsheet" type="radio" name="items" value="3" <?php if(isset($radioitem) && $radioitem  == 3){ echo 'checked';} ?>>
                      <label for="bookingsheet">Booking Sheet</label>
                      </li>
                      <li>
                      <input id="vendor" type="radio" name="items" value="4" <?php if(isset($radioitem) && $radioitem  == 4){ echo 'checked';} ?>> 
                      <label for="vendor">Vendor</label>
                      </li>
                      <li>
                      <input id="buyer" type="radio" name="items" value="5" <?php if(isset($radioitem) && $radioitem  == 5){ echo 'checked';} ?>> 
                      <label for="buyer">Buyer</label>
                      </li>
                      </ul>
                      <?php } ?></div>
                      
                      <div class="search_block row">
              <div class="col-sm-6"> 
              <div class="form-group"> 
              <label>Breed: </label><select name="breed"><option value="">All</option>   <?php foreach($Breed as $data)
                { ?><option value="<?php echo $data; ?>" <?php if(isset($s_breed) && $s_breed == $data){ echo 'selected';} ?> ><?php echo $data; ?></option><?php } ?></select>
                </div>
                </div>
                <div class="col-sm-6">
                <div class="form-group">
                <label>Type: </label><select name="type"><option value="">All</option>    <?php foreach($Type as $data)
                { ?><option value="<?php echo $data; ?>" <?php if(isset($s_type) && $s_type == $data){ echo 'selected';} ?> ><?php echo $data; ?></option> <?php } ?></select>
                </div>
                </div>
                <div class="col-sm-6"> 
                <div class="form-group"> 
                <label>Sale Date From: </label>
                <div id="datepickerfrom" class="input-group date" data-date-format="dd MM yyyy">
                    <input class="form-control" type="text" name="salefrom" value="<?php if(isset($s_sale_from)){echo date_format(date_create($s_sale_from),'d F Y');}else{echo date_format(date_create($Currentweeksale[0]->Sale_Date),'d F Y');} ?>" readonly />
                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                </div>
                </div>
                </div>
                    <div class="col-sm-6"> 
                    <div class="form-group"> 
                    <label>To: </label>
                <div id="datepickerto" class="input-group date" data-date-format="dd MM yyyy">
                    <input class="form-control" type="text" name="saleto" value="<?php if(isset($s_sale_to)){echo date_format(date_create($s_sale_to),'d F Y');}else{echo date('d F Y');} ?>" readonly />
                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                </div>
                </div>
                </div>
                <input type="submit" name="submitreport" value="search" class="btn btn-primary">
                </div>
                </form>


        <form class="form-horizontal" action="<?php echo PLD_URL.'/generate_csv.php'; ?>" method="post" name="upload_excel"   
                  enctype="multipart/form-data">
              <div class="form-group text-right">
                        <div class="col-md-12">
                            <input type="submit" name="Export" class="btn btn-success" value="Export"/>
                        </div>
               </div>                    
        </form>   
                
                </div>
              <table id="marketreport12" class="table" >

                <thead>
        <tr>
            <th>Sale Date</th>
            <th>Type</th>
            <th>Breed</th>
            <th>Sale Method</th>
            <th class="right">Lot</th>
            <th class="right">Head</th>
            <th class="right">c/kg</th>
            <th class="right">Avg kg</th>
            <th class="right">Avg Amt</th>
            <th class="right">Tot kg</th>
            <th class="right">Tot Amt</th> 
            <?php
            $radioitem = $_GET['items'];
            if($radioitem ==1){
            echo "<th>Buyer Code</th>";
            echo "<th>Agent ID</th> ";
            } 
            if($radioitem ==3){
            echo "<th>Buyer Code</th>";
            echo "<th>Temp Code</th> ";
            } 
            if($radioitem ==4){
            echo "<th>Temp Code</th> ";
            } 
            ?>         
            
        </tr>
    </thead>
                <tbody>
                  
                <?php 
                  $sellerid= array();
                  $all = 0;
                 
                  $newarray= array();
                  if($radioitem ==3){
                      $sname="Seller Id: ";
                  foreach($Currentweeksale as $data){
                   $sellid= str_replace(' ', '_', $data->Seller_ID);
                      $newarray[$sellid][]=$data;
                  } }
                  
                   else if($radioitem ==4){
                      $sname="Seller Id: ";
                  foreach($Currentweeksale as $data){
                   $sellid= str_replace(' ', '_', $data->Seller_ID);
                      $newarray[$sellid][]=$data;
                  } }
                   else if($radioitem ==5){
                      
                  foreach($Currentweeksale as $data){
                      $byrid = $data->Buyer_ID.'#@#'.$data->Buyer_Code;
                      
                   $buyid= str_replace(' ', '_', $byrid);
                      $newarray[$buyid][]=$data;
                  } }
                  
                  else {
                      $all = 1;
                       foreach($Currentweeksale as $data){
                      $newarray[][]=$data;
                       }
                  }
                  // echo "<pre>"; print_r($newarray); die;
                   
                   foreach($newarray as $key=>$value){ 
                       if($all == 0){
                        echo "<tr>";
                        if($radioitem ==5){
                            $byidcode=explode('#@#',$key);
                            
                            echo "<td>".str_replace('_', ' ', $byidcode[1])."</td>";
                         echo "<td colspan='3'>".str_replace('_', ' ', $byidcode[0])."</td>";
                            
                        }else{
                     echo "<td colspan='3'>$sname ".str_replace('_', ' ', $key)."</td>"; }
                     echo "</tr>"; }
                     
                     $head=0;
                     $totalamount=0;
                     foreach($value as $data){
                        echo "<tr>";
        echo "<td>".date_format(date_create($data->Sale_Date),'d M Y')."</td>";
        //echo "<td>".$data->Sale_Date."</td>";
        echo "<td>".$data->Type."</td>";
        echo "<td>".$data->Breed."</td>";
        echo "<td>".$data->Sale_Method."</td>";            
        echo "<td class='right'>".$data->Lot."</td>";
        echo "<td class='right'>".$data->Head."</td>";
        echo "<td class='right'>".$data->c_kg."</td>";
        echo "<td class='right'>".number_format($data->Avg_kg,1)."</td>";
        echo "<td class='right'>".number_format($data->Avg_Amt,2)."</td>";
        echo "<td class='right'>".number_format($data->Tot_kg,1)."</td>";
        echo "<td class='right'>".number_format($data->Tot_Amt,2)."</td>";
        if($radioitem ==1){
            echo "<td>".$data->Buyer_Code."</td>";
            echo "<td>".$data->Agent_ID."</td> ";
        } 
        if($radioitem ==3){
            echo "<td>".$data->Buyer_Code."</td>";
            echo "<td>".$data->Temp_Code."</td> ";
         }
         if($radioitem ==4){
            echo "<td>".$data->Temp_Code."</td> ";
         }
         echo "</tr>";
         
         
         $head+=$data->Head;
         $totalamount+=$data->Tot_Amt;
                     }
                     
                    if($all == 0){ echo "<tr>";
        echo "<td></td>";
        echo "<td></td>";
        echo "<td></td>";
        echo "<td></td>";            
        echo "<td class='right'></td>";
        echo "<td class='right'>".$head."</td>";
        echo "<td class='right'></td>";
        echo "<td class='right'></td>";
        echo "<td class='right'></td>";
        echo "<td class='right'></td>";
        echo "<td class='right'>".number_format($totalamount,2)."</td>";
            echo "<td></td>";
            echo "<td></td> ";
         echo "</tr>";
                    }
         
                       
                   }
                  
        
        if (empty($Currentweeksale)) {
            echo "<tr>";
        echo "<td></td>";
        //echo "<td>".$data->Sale_Date."</td>";
        echo "<td></td>";
        echo "<td></td>";
        echo "<td></td>";            
        echo "<td class='right'></td>";
        echo "<td class='right'>0</td>";
        echo "<td class='right'></td>";
        echo "<td class='right'></td>";
        echo "<td class='right'></td>";
        echo "<td class='right'></td>";
        echo "<td class='right'></td>";
        if($radioitem ==1){
            echo "<td></td>";
            echo "<td></td> ";
        } 
        if($radioitem ==3){
            echo "<td></td>";
            echo "<td></td> ";
         }
         if($radioitem ==4){
            echo "<td></td> ";
         }
         echo "</tr>";

     
        }
        if($paging_info['pages'] == $paging_info['curr_page']){
        if($headtotal !='' && ($radioitem ==3 || $radioitem ==4 || $radioitem ==5)){
        echo"<tr>";
        echo "<td></td>";
        echo "<td></td>";
        echo "<td></td>";
        echo "<td></td>";            
        echo "<td class='right'></td>";
        echo "<td class='right'>".$headtotal."</td>";
        echo "<td class='right'></td>";
        echo "<td class='right'></td>";
        echo "<td class='right'></td>";
        echo "<td class='right'></td>";
        echo "<td class='right'>".number_format($totalamt,2)."</td>";
            echo "<td></td>";
            echo "<td></td> ";
         echo "</tr>";
       }
     }
    echo "</tbody>";
echo "</table>";

       ?>
       <?php $count = $rowcount; ?>

   <!-- Call our function from above -->
   <?php //$paging_info = get_paging_info($count,$per_page,$urlpageno); ?>


   <div class="pagination_report">
       <!-- If the current page is more than 1, show the First and Previous links -->
       <?php if($paging_info['curr_page'] > 1) : ?>
           <a href='<?php echo $queryString;?>start=<?php echo 0;?>' title='Page 1'>First</a>
           <a href='<?php echo $queryString;?>start=<?php echo $per_page*($paging_info['curr_page']-2); ?>'  title='Page <?php echo ($paging_info['curr_page'] - 1); ?>'>Prev</a>
       <?php endif; ?>



       <?php
           //setup starting point

           //$max is equal to number of links shown
           $max = 7;
           if($paging_info['curr_page'] < $max)
               $sp = 1;
           elseif($paging_info['curr_page'] >= ($paging_info['pages'] - floor($max / 2)) )
               $sp = $paging_info['pages'] - $max + 1;
           elseif($paging_info['curr_page'] >= $max)
               $sp = $paging_info['curr_page']  - floor($max/2);
       ?>

       <!-- If the current page >= $max then show link to 1st page -->
       <?php if($paging_info['curr_page'] >= $max) : ?>

           <a href='<?php echo $queryString;?>start=<?php echo 0; ?>' title='Page 1'>1</a>
           ..

       <?php endif; ?>

       <!-- Loop though max number of pages shown and show links either side equal to $max / 2 -->
       <?php for($i = $sp; $i <= ($sp + $max -1);$i++) : ?>

           <?php
               if($i > $paging_info['pages'])
                   continue;
           ?>

           <?php if($paging_info['curr_page'] == $i) : ?>

               <span class='bold'><?php echo $i; ?></span>
               

           <?php else : ?>

               <a href='<?php echo $queryString;?>start=<?php echo $per_page*($i-1); ?>' title='Page <?php echo $i; ?>'><?php echo $i; ?></a>

           <?php endif; ?>

       <?php endfor; ?>


       <!-- If the current page is less than say the last page minus $max pages divided by 2-->
       <?php if($paging_info['curr_page'] < ($paging_info['pages'] - floor($max / 2))) : ?>

           ..
           <a href='<?php echo $queryString;?>start=<?php echo $per_page*($paging_info['pages']-1); ?>' title='Page <?php echo $paging_info['pages']; ?>'><?php echo $paging_info['pages']; ?></a>

       <?php endif; ?>

       <!-- Show last two pages if we're not near them -->
       <?php if($paging_info['curr_page'] < $paging_info['pages']) : ?>

           <a href='<?php echo $queryString;?>start=<?php echo $per_page*$paging_info['curr_page']; ?>'  title='Page <?php echo ($paging_info['curr_page'] + 1); ?>'>Next</a>

           <a href='<?php echo $queryString;?>start=<?php echo $per_page*($paging_info['pages']-1); ?>' title='Page <?php echo $paging_info['pages']; ?>'>Last</a>

       <?php endif; ?>
   </div> 
<?php
echo "</div>";
echo "</div>";
echo "</div>";
echo "</body>";
echo "</html>";
}



//Add ShortCode for "front end"
add_shortcode('market_data', 'market_data_front');
?>
